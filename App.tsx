import React, { useState } from 'react'
import { StatusBar } from 'react-native'
import { Provider } from 'react-redux'
import { AppLoading } from 'expo'
import store from './src/store'
import { AppNavigation } from './src/navigation/AppNavigation'
import { bootstrap } from './src/bootstrap'
import './src/i18n'
import { isIos } from './src/lib/platform'
import { AppPopupNotification } from './src/components/ui/AppPopupNotification'

export default function App() {
  const [isReady, setInitStatus] = useState(false)
  const barStyle = isIos ? 'dark-content' : 'light-content'

  if (!isReady) {
    return (
      <AppLoading
        startAsync={bootstrap}
        onFinish={() => setInitStatus(true)}
        onError={console.error}
      />
    )
  }

  return (
    <Provider store={store}>
      <StatusBar barStyle={barStyle} />
      <AppNavigation />
      <AppPopupNotification />
    </Provider>
  )
}
