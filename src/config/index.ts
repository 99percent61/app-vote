export default {
  api: {
    // url: 'http://188.120.225.51:8081/api', // prod
    url: 'http://192.168.1.69:8081/api', // local
  },
  filesApi: {
    url: 'http://188.120.225.51:8082/api', // prod
    // url: 'http://192.168.1.69:8082/api', // local
  },
  i18n: {
    defaultLocale: 'en',
  },
}
