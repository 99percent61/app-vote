import * as Font from 'expo-font'
import logger from './lib/logger'
import NunitoRegularFont from '../assets/fonts/Nunito-Regular.ttf'
import NunitoBoldFont from '../assets/fonts/Nunito-Bold.ttf'

export async function bootstrap() {
  try {
    await Font.loadAsync({
      'nunito-regular': NunitoRegularFont,
      'nunito-bold': NunitoBoldFont,
    })
  } catch (error) {
    logger.error(error)
  }
}
