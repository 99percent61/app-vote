import Task from '../lib/task'
import { Task as TaskInterface } from '../lib/task/types'

export class Complaint {
  static async createReport(complaint: { poll_id: number, type: string }): Promise<TaskInterface> {
    const result = await Task({ url: '/complaint/create', method: 'PUT', body: complaint })
    return result
  }
}
