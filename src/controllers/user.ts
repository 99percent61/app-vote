import Task from '../lib/task'
import { Task as TaskInterface } from '../lib/task/types'
import { RegistrationUser } from '../store/modules/user/types'

class User {
  public createSession = async (
    { login, password }: { login: string, password: string },
  ): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/session/create', method: 'POST', body: { login, password } })
    return result
  }

  public me = async (): Promise<TaskInterface> => {
    return Task({ url: '/user/me', method: 'GET' })
  }

  public registerUser = async (payload: RegistrationUser): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/registration', method: 'POST', body: payload })
    return result
  }

  public sendCodeForRepairPass = async ({ email }: { email: string }): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/password/forgot', method: 'POST', body: { email } })
    return result
  }

  public confirmCode = async (data: { email: string, code: string }): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/password/confirm-code', method: 'POST', body: data })
    return result
  }

  public changePasswordByToken = async (
    data: { email: string, password: string, repeatedPassword: string, token: string },
  ): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/password/create-new-pass-by-token', method: 'POST', body: data })
    return result
  }

  public changePasswordByPass = async (
    data: { currentPassword: string, newPassword: string, repeatedPassword: string },
  ): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/password/create-new-pass-by-pass', method: 'POST', body: data })
    return result
  }

  public update = async (data: { avatar: string }): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/update', method: 'PUT', body: data })
    return result
  }

  public sendFeedback = async (
    data: { title: string, email: string, message: string },
  ): Promise<TaskInterface> => {
    const result = await Task({ url: '/user/feedback', method: 'POST', body: data })
    return result
  }

  public acceptTerms = async (): Promise<TaskInterface> => {
    const result = await Task({ url: 'user/terms', method: 'PUT' })
    return result
  }
}

export default new User()
