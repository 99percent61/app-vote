function buildUrlWithQueryFromParams(url: string, params: Record<string, any>): string {
  let localUrl = url
  const paramList: string[] = []
  Object.keys(params).forEach((key) => {
    paramList.push(`${key}=${params[key]}`)
  })
  localUrl = localUrl.includes('?') ? `${localUrl}${paramList.join('&')}` : `${localUrl}?${paramList.join('&')}`
  return localUrl
}

export {
  buildUrlWithQueryFromParams,
}
