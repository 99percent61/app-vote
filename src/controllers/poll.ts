import Task, { FileTask } from '../lib/task'
import { Task as TaskInterface } from '../lib/task/types'
import { Poll as PollInterface, QueryListParams } from '../store/modules/poll/types'
import { buildUrlWithQueryFromParams } from './helpers'

class Poll {
  public createPoll = async (poll: PollInterface): Promise<TaskInterface> => {
    const result = await Task({ url: '/poll/create', method: 'PUT', body: poll })
    return result
  }

  public sendFile = async (files: any): Promise<TaskInterface> => {
    const result = await FileTask({ url: '/poll/files/image', method: 'POST', files })
    return result
  }

  public list = async (params: QueryListParams): Promise<TaskInterface> => {
    const url = buildUrlWithQueryFromParams('/poll/list', params)
    const result = await Task({ url, method: 'GET' })
    return result
  }

  public ownList = async (params: QueryListParams): Promise<TaskInterface> => {
    const url = buildUrlWithQueryFromParams('/poll/list/own', params)
    const result = await Task({ url, method: 'GET' })
    return result
  }

  public ownSingleById = async (id: number): Promise<TaskInterface> => {
    const url = buildUrlWithQueryFromParams('/poll/list/own-by-id', { id })
    const result = await Task({ url, method: 'GET' })
    return result
  }

  public vote = async (body: { poll_id: number, answer_id: number }): Promise<TaskInterface> => {
    const result = await Task({ url: '/poll/vote', method: 'PUT', body })
    return result
  }

  public delete = async ({ id }: { id: number }): Promise<TaskInterface> => {
    const result = await Task({ url: '/poll/delete', method: 'DELETE', body: { id } })
    return result
  }

  public update = async (
    { id, is_expired }: { id: number, is_expired: 1 | 0 },
  ): Promise<TaskInterface> => {
    const result = await Task({ url: '/poll/update', method: 'PUT', body: { id, is_expired } })
    return result
  }

  public statisticsByPollId = async (
    { pollId }: { pollId: number },
  ): Promise<TaskInterface> => {
    const url = buildUrlWithQueryFromParams('/poll/statistics/list-by-poll', { pollId })
    const result = await Task({ url, method: 'GET' })
    return result
  }
}

export default new Poll()
