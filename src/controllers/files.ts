import { FileTask } from '../lib/task'
import { Task as TaskInterface } from '../lib/task/types'

class Files {
  public uploadImages = async (files: any): Promise<TaskInterface> => {
    const result = await FileTask({ url: '/image/upload', method: 'POST', files })
    return result
  }
}

export default new Files()
