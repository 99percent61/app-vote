import * as Localization from 'expo-localization'
import i18n from 'i18n-js'
import en from './translations/en'
import ru from './translations/ru'
import config from '../config'

const { defaultLocale } = config.i18n

const splitLocale = (rawLocale: string): string => {
  const [locale] = [...rawLocale.split('-')]
  return locale
}

i18n.fallbacks = defaultLocale
i18n.locale = splitLocale(Localization.locale)
i18n.defaultLocale = defaultLocale

i18n.translations = {
  en,
  ru,
}
