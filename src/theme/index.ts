export const THEME = {
  MAIN_COLOR: '#000',
  // MAIN_COLOR: '#303f9f',
  DANGER_COLOR: '#d81b60',
  colors: {
    WHITE: '#fff',
    GRAY: '#e0e0e0',
    GRAY2: '#999999',
    BLACK: '#000',
    BLUE: '#1c84d9',
  },
  text: {
    error: {
      color: '#d81b60',
      textAlign: 'center',
      marginBottom: 5,
      marginTop: 5,
    },
  },
}
