import React, { useState, useEffect } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native'
import { SliderBox } from 'react-native-image-slider-box'
import { THEME } from '../../theme'
import logger from '../../lib/logger'

interface Props {
  images: string[]
  dotColor?: string
  initialSliderHeight?: number
  fixedHeight?: boolean
  parentWidth?: number
  style?: Record<string, any>
}

const deviceWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    backgroundColor: 'black',
  },
})

export const ImageCarousel: React.FC<Props> = (
  {
    images,
    dotColor = THEME.colors.BLUE,
    initialSliderHeight = 300,
    fixedHeight = false,
    parentWidth = deviceWidth,
    style = {},
  },
) => {
  const [sliderHeight, setSliderHeight] = useState<number>(initialSliderHeight)
  let isUnmountComponent = false

  const changeSliderHeight = (index: number): void => {
    Image.getSize(
      images[index],
      (width: number, height: number) => {
        const proportion = height / width
        if (!isUnmountComponent) {
          setSliderHeight(deviceWidth * proportion)
        }
      },
      logger.error,
    )
  }

  useEffect(() => {
    if (!fixedHeight) changeSliderHeight(0)
    return () => {
      isUnmountComponent = true
    }
  }, [])

  return (
    <View style={styles.root}>
      <SliderBox
        images={images}
        sliderBoxHeight={sliderHeight}
        dotColor={dotColor}
        resizeMode="contain"
        ImageComponentStyle={{ ...styles.image, ...style }}
        parentWidth={parentWidth}
      />
    </View>
  )
}
