import React, { memo, useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import { Card, IconButton, Divider } from 'react-native-paper'
import i18n from 'i18n-js'
import { Poll, Answer } from '../../store/modules/poll/types'
import { AppText } from '../ui/AppText'
import { THEME } from '../../theme'
import { AppTextBold } from '../ui/AppTextBold'
import { ImageCarousel } from '../image/ImageCarousel'
import { vote as voteAction } from '../../store/modules/poll/actions'
import { AppProgressBar } from '../ui/AppProgressBar'
import { isIos } from '../../lib/platform'
import { AppModal } from '../ui/AppModal'
import { ComplaintTypes } from '../../store/modules/complaint/types'
import { createComplaint } from '../../store/modules/complaint/actions'
import { toggleNotification } from '../../store/modules/ui/actions'

interface Props {
  poll: Poll
}

const deviceWidth = Dimensions.get('window').width
const MORE_ICON = isIos ? 'dots-horizontal' : 'dots-vertical'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: THEME.colors.WHITE,
    marginBottom: 15,
    borderRadius: 5,
    overflow: 'hidden',
  },
  imageWrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    width: deviceWidth,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
  },
  contentWrapper: {
    paddingTop: 5,
    padding: 20,
  },
  author: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarWrapper: {
    borderRadius: 50,
    overflow: 'hidden',
  },
  avatar: {
    flex: 1,
    width: 40,
    height: 40,
  },
  login: {
    color: THEME.colors.GRAY2,
    fontSize: 14,
  },
  title: {
    textAlign: 'center',
    marginBottom: 10,
  },
  answerWrapper: {
    position: 'relative',
  },
  answer: {
    flex: 1,
    position: 'relative',
    width: '100%',
    height: 50,
    borderWidth: 1,
    borderColor: THEME.colors.GRAY,
    borderRadius: 5,
    marginBottom: 10,
  },
  answerTitleWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    paddingHorizontal: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  answerTitle: {
    color: THEME.colors.WHITE,
  },
  answerLine: {
    width: 7,
    height: '100%',
    backgroundColor: THEME.colors.BLUE,
    marginRight: 10,
    borderRadius: 5,
  },
  reportType: {
    width: '100%',
    height: 35,
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingTop: isIos ? 8 : 0,
    flex: 1,
  }
})

function areEqualPoll(prevPoll: Poll, nextPoll: Poll): boolean {
  if (prevPoll.answers.length !== nextPoll.answers.length) return false
  if (prevPoll.is_expired !== nextPoll.is_expired) return false

  return prevPoll.answers.every((prevAnswer) => {
    const nextAnswer = nextPoll.answers.find((answ) => answ.id === prevAnswer.id)
    if (nextAnswer === undefined) return false
    if (prevAnswer.votes !== nextAnswer.votes) return false
    return true
  })
}

function areEqual(prevProps: Props, nextProps: Props): boolean {
  return prevProps.poll.id === nextProps.poll.id && areEqualPoll(prevProps.poll, nextProps.poll)
}

const PollListItem: React.FC<Props> = ({ poll }) => {
  const [localPoll, setLocalPoll] = useState<Poll>(poll)
  const [answers, setAnswers] = useState<Answer[]>(poll.answers)
  const [isVisibleMenuModal, setVisibilityMenuModal] = useState<boolean>(false)
  const [isVisibleReportOptionsModal, setVisibilityReportOptionsModal] = useState<boolean>(false)

  useEffect(() => {
    setLocalPoll(poll)
    setAnswers(poll.answers)
  }, [poll])

  const dispatch = useDispatch()
  const imageBlock = () => {
    if (!localPoll.image || !localPoll.image.length) {
      return null
    }

    return (
      <ImageCarousel images={localPoll.image || []} />
    )
  }

  const getUserAvatar = () => {
    let avatar = require('../../../assets/person.png')
    if (localPoll.author && localPoll.author.avatar) {
      avatar = {
        uri: localPoll.author.avatar,
      }
    }
    return avatar
  }

  const vote = async (answerId: number): Promise<void> => {
    const target = answers.find((answ) => answ.id === answerId)
    if (target) {
      const votes = (target.votes || 0) + 1
      const copyAnswers = answers.slice()
      copyAnswers.forEach((answ) => {
        if (answ.id === answerId) {
          answ.votes = votes
        }
      })
      const copyPoll = { ...localPoll, user_vote: answerId }
      setLocalPoll(copyPoll)
      setAnswers(copyAnswers)
      await dispatch(voteAction({
        poll_id: localPoll.id,
        answer_id: answerId,
      }))
    }
  }

  const calculateProgress = (answer: Answer): number => {
    const voteCount = answers.reduce((acc, item) => {
      return acc + (item.votes || 0)
    }, 0)
    const currentVotes = answer.votes || 0
    const percents = currentVotes / (voteCount || 1)
    return percents
  }

  const renderAnswers = () => answers.map((answer) => {
    if (localPoll.user_vote) {
      const answerStyles = localPoll.user_vote === answer.id
        ? { ...styles.answerTitle, fontWeight: '700' }
        : styles.answerTitle
      const progress = calculateProgress(answer)

      return (
        <AppProgressBar
          key={answer.id}
          answer={answer}
          progress={progress}
          answerStyles={answerStyles}
        />
      )
    }

    return (
      <TouchableOpacity
        key={answer.id}
        style={styles.answer}
        onPress={() => vote(answer.id)}
      >
        <View style={styles.answerLine} />
        <View style={styles.answerTitleWrapper}>
          <AppText>
            { answer.title }
          </AppText>
        </View>
      </TouchableOpacity>
    )
  })

  const iconButton = (props: any) => (<IconButton {...props} icon={MORE_ICON} onPress={() => setVisibilityMenuModal(true)} />)

  const openReportOptions = (): void => {
    setVisibilityMenuModal(false)
    setTimeout(() => {
      setVisibilityReportOptionsModal(true)
    }, 400)
  }

  const sendReport = (poll_id: number, type: string): void => {
    dispatch<any>(createComplaint({ poll_id, type }))
    setVisibilityReportOptionsModal(false)
    dispatch(toggleNotification({
      isVisible: true,
      message: i18n.t('ReportSent'),
    }))
  }

  const renderReportOptions = () => {
    return Object.values(ComplaintTypes).map((type) => (
      <View
        key={type}
        style={{ borderBottomWidth: 1, width: '100%', borderBottomColor: THEME.colors.GRAY }}
      >
        <TouchableOpacity
          onPress={() => sendReport(localPoll.id, type)}
        >
          <AppText style={styles.reportType}>
            { i18n.t(type) }
          </AppText>
        </TouchableOpacity>
      </View>
    ))
  }

  const renderMenuModal = () => (
    <AppModal
      isVisible={isVisibleMenuModal}
      setVisibility={setVisibilityMenuModal}
      modalHeight="20%"
    >
      <TouchableOpacity onPress={openReportOptions}>
        <AppText style={{ alignSelf: 'center', color: THEME.DANGER_COLOR, height: 30 }}>
          { i18n.t('Report') }
        </AppText>
      </TouchableOpacity>
      <Divider style={{ marginBottom: 5 }} />
    </AppModal>
  )

  const renderReportOptionsModal = () => (
    <AppModal
      isVisible={isVisibleReportOptionsModal}
      setVisibility={setVisibilityReportOptionsModal}
    >
      <ScrollView>
        { renderReportOptions() }
      </ScrollView>
    </AppModal>
  )

  return (
    <View style={styles.root}>
      { imageBlock() }
      <View style={styles.contentWrapper}>
        <View style={styles.author}>
          <Card.Title
            style={{ marginHorizontal: -15, marginVertical: 0 }}
            title={localPoll.author.login}
            titleStyle={styles.login}
            left={(props) => (
              <Image
                {...props}
                source={getUserAvatar()}
                style={styles.avatar}
              />
            )}
            leftStyle={styles.avatarWrapper}
            right={iconButton}
          />
        </View>
        <AppTextBold style={styles.title}>
          { localPoll.title }
        </AppTextBold>
        <View style={styles.answerWrapper}>
          { renderAnswers() }
        </View>
      </View>
      { renderMenuModal() }
      { renderReportOptionsModal() }
    </View>
  )
}

export default memo(PollListItem, areEqual)
