import React, { useEffect, useRef, useState } from 'react'
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import i18n from 'i18n-js'
import { Poll } from '../../store/modules/poll/types'
import PollListItem from './PollListItem'
import { AppText } from '../ui/AppText'
import { THEME } from '../../theme'

interface Props {
  list: Poll[]
  refreshing: boolean
  onRefresh: () => Promise<void>
  onEndReached: () => void
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 10,
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatList: {
    paddingVertical: 10,
  },
  activityIndicator: {
    marginTop: 30,
  },
})

export const PollList: React.FC<Props> = ({
  list,
  refreshing,
  onRefresh,
  onEndReached,
}) => {
  const navigation = useNavigation()
  const flatList = useRef(null)
  const [isFocused, setIsFocused] = useState<boolean>(false)

  const pollItem = ({ item }: { item: Poll }) => (
    <PollListItem poll={item} />
  )

  useEffect(() => {
    const unsubscribe = navigation.dangerouslyGetParent().addListener('tabPress', (e) => {
      if (flatList.current && isFocused) {
        flatList.current.scrollToIndex({
          index: 0,
        })
      }
    })

    return unsubscribe
  }, [navigation, flatList, isFocused])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => setIsFocused(true))
    return unsubscribe
  }, [navigation])

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => setIsFocused(false))
    return unsubscribe
  }, [navigation])

  const renderContent = () => {
    if (list.length === 0 && refreshing) {
      return (
        <ActivityIndicator
          style={styles.activityIndicator}
          size="large"
          color={THEME.colors.BLUE}
        />
      )
    }
    if (list.length > 0) {
      return (
        <FlatList
          ref={flatList}
          data={list}
          renderItem={pollItem}
          keyExtractor={(item) => String(item.id)}
          style={styles.flatList}
          onEndReachedThreshold={5}
          onEndReached={onEndReached}
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      )
    }
    return (
      <View style={styles.empty}>
        <AppText>{ i18n.t('ListEmpty') }</AppText>
      </View>
    )
  }

  return (
    <View style={styles.root}>
      { renderContent() }
    </View>
  )
}
