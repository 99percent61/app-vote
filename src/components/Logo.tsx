import React from 'react'
import { StyleSheet, View, Image } from 'react-native'

const Logotip = require('../../assets/Logo.png')

interface Props {
  style?: Record<string, any>
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 150,
  },
})

export const Logo: React.FC<Props> = ({ style } = { style: {} }) => (
  <View style={styles.root}>
    <Image
      source={Logotip}
      style={{ ...styles.image, ...style }}
      resizeMode="contain"
    />
  </View>
)
