import React from 'react'
import {
  View,
  StyleSheet,
  Image,
} from 'react-native'
import { useSelector } from 'react-redux'
import { RootState } from '../../store'
import { User } from '../../store/modules/user/types'
import { AppTextBold } from '../ui/AppTextBold'
import { isAndroid } from '../../lib/platform'

interface Props {}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: isAndroid ? -45 : 0,
  },
  avatar: {
    borderRadius: 50,
    width: 30,
    height: 30,
    marginRight: 10,
  },
  title: {
    fontSize: 20,
  },
})

export const AccountLogo: React.FC<Props> = () => {
  const user = useSelector<RootState, User>((state) => state.user.current)

  const getUserAvatar = () => {
    let avatar = require('../../../assets/person.png')
    if (user && user.avatar) {
      avatar = {
        uri: user.avatar,
      }
    }
    return avatar
  }

  return (
    <View style={styles.container}>
      <Image
        source={getUserAvatar()}
        style={styles.avatar}
      />
      <AppTextBold style={styles.title}>
        { user && user.name }
      </AppTextBold>
    </View>
  )
}
