import React from 'react'
import { StyleSheet, Image, View } from 'react-native'
import { useSelector } from 'react-redux'
import { RootState } from '../../store'
import { User } from '../../store/modules/user/types'
import { AppIcon } from '../ui/AppIcon'
import { THEME } from '../../theme'

interface Props {
  color: string
  size: number
  focused: boolean
}

const styles = StyleSheet.create({
  avatarWrapper: {
    borderRadius: 50,
    overflow: 'hidden',
    padding: 1,
  },
  avatarBorder: {
    borderWidth: 1,
    borderColor: THEME.colors.BLUE,
  },
  avatar: {
    borderRadius: 50,
  },
})

export const BottomLogo: React.FC<Props> = ({ color, size, focused }) => {
  const user = useSelector<RootState, User>((state) => state.user.current)

  const getIcon = () => {
    if (user && user.avatar) {
      const wrapperStyles = (() => (
        focused ? { ...styles.avatarWrapper, ...styles.avatarBorder } : styles.avatarWrapper
      ))()
      return (
        <View style={wrapperStyles}>
          <Image
            source={{ uri: user.avatar }}
            style={{ ...styles.avatar, width: size, height: size }}
          />
        </View>
      )
    }

    return (
      <AppIcon name="account-circle" color={color} size={size} />
    )
  }

  return getIcon()
}
