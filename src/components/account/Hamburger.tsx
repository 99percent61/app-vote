import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { AppIcon } from '../ui/AppIcon'
import { AccountProps } from '../../screens/types'
import { THEME } from '../../theme'

interface Props extends AccountProps {}

const styles = StyleSheet.create({
  root: {
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
})

export const AccountHamburger: React.FC<Props> = ({ navigation }) => (
  <TouchableOpacity
    onPress={() => navigation.navigate('AccountSettingList')}
    style={styles.root}
  >
    <AppIcon
      name="menu"
      color={THEME.colors.BLACK}
      size={30}
    />
  </TouchableOpacity>
)
