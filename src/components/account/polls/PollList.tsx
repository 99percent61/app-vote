import React from 'react'
import { View, FlatList, StyleSheet, Dimensions } from 'react-native'
import i18n from 'i18n-js'
import { Poll } from '../../../store/modules/poll/types'
import PollListItem from './PollListItem'
import { AppText } from '../../ui/AppText'

interface Props {
  list: Poll[]
  refreshing: boolean
  onRefresh: () => Promise<void>
  onEndReached: () => void
}

const deviceHeight = Dimensions.get('window').height
const styles = StyleSheet.create({
  root: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 10,
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: deviceHeight / 2 - 100,
  },
  flatList: {
    paddingVertical: 10,
  },
  items: {
    justifyContent: 'space-between',
  },
})

export const AccountPollList: React.FC<Props> = ({
  list,
  refreshing,
  onRefresh,
  onEndReached,
}) => {
  const pollItem = ({ item }: { item: Poll }) => (
    <PollListItem poll={item} />
  )

  return (
    <View style={styles.root}>
      <FlatList
        data={list}
        renderItem={pollItem}
        keyExtractor={(item) => String(item.id)}
        numColumns={2}
        style={styles.flatList}
        columnWrapperStyle={styles.items}
        onEndReachedThreshold={0}
        onEndReached={onEndReached}
        refreshing={refreshing}
        onRefresh={onRefresh}
        ListEmptyComponent={() => (
          <View style={styles.empty}>
            <AppText>{ i18n.t('ListEmpty') }</AppText>
          </View>
        )}
      />
    </View>
  )
}
