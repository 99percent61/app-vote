import React, { useState, useEffect } from 'react'
import { View, StyleSheet } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigation, useRoute } from '@react-navigation/native'
import { Poll } from '../../../store/modules/poll/types'
import { AccountPollList } from './PollList'
import { RootState } from '../../../store'
import { ownList } from '../../../store/modules/poll/actions'
import { AccountScreenRouteProp } from '../../../screens/types/account'

interface Props {}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export const InActivePolls: React.FC<Props> = () => {
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [polls, setPolls] = useState<Poll[]>([])
  const dispatch = useDispatch()
  const total = useSelector<RootState, number>((state) => state.poll.ownList.total)
  const pageSize = useSelector<RootState, number>((state) => state.poll.page.size)
  const route = useRoute<AccountScreenRouteProp>()
  const navigation = useNavigation()

  const loadPolls = async (
    {
      searchAfter = 999999999999999,
      replaceState = true,
      forceRefresh = true,
      isExpired = '0',
    }: {
      searchAfter?: number,
      replaceState?: boolean,
      forceRefresh?: boolean,
      isExpired?: string,
    } = {},
  ): Promise<void> => {
    if (total <= pageSize && !forceRefresh) return
    setRefreshing(true)
    const result = await dispatch<any>(ownList({
      size: pageSize,
      sort: 'id:desc',
      isExpired,
      searchAfter,
    }, replaceState))
    setPolls(result)
    setRefreshing(false)
  }

  const onRefresh = (): Promise<void> => (
    loadPolls({ isExpired: '1' })
  )

  const onEndReached = (): void => {
    const lastPoll = polls.slice().pop()
    if (lastPoll && lastPoll.id > 1) {
      loadPolls({
        searchAfter: lastPoll.id,
        replaceState: false,
        forceRefresh: false,
        isExpired: '1',
      })
    }
  }
  useEffect(() => {
    loadPolls({ isExpired: '1' })
  }, [])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (route.params.updatePollList) {
        loadPolls({ isExpired: '1' })
        navigation.setParams({ updatePollList: false })
      }
    })

    return unsubscribe
  }, [navigation, route])

  return (
    <View style={styles.root}>
      <AccountPollList
        list={polls}
        refreshing={refreshing}
        onRefresh={onRefresh}
        onEndReached={onEndReached}
      />
    </View>
  )
}
