import React, { memo } from 'react'
import {
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { Poll, Answer } from '../../../store/modules/poll/types'
import { THEME } from '../../../theme'
import { AppTextBold } from '../../ui/AppTextBold'

interface Props {
  poll: Poll
}

const { width } = Dimensions.get('window')
const cardWidth = (width - 30) / 2

const styles = StyleSheet.create({
  root: {
    borderRadius: 10,
    width: cardWidth,
    height: cardWidth,
    overflow: 'hidden',
    marginBottom: 10,
    backgroundColor: THEME.colors.WHITE,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    position: 'relative',
  },
  placeholder: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: THEME.colors.BLACK,
    opacity: 0.5,
    position: 'absolute',
  },
  title: {
    color: THEME.colors.WHITE,
    lineHeight: 24,
    fontSize: 17,
    padding: 10,
    textAlign: 'center',
  },
})

function areEqualPoll(prevPoll: Poll, nextPoll: Poll): boolean {
  if (prevPoll.answers.length !== nextPoll.answers.length) return false
  if (prevPoll.is_expired !== nextPoll.is_expired) return false

  return prevPoll.answers.every((prevAnswer) => {
    const nextAnswer = nextPoll.answers.find((answ) => answ.id === prevAnswer.id)
    if (nextAnswer === undefined) return false
    if (prevAnswer.votes !== nextAnswer.votes) return false
    return true
  })
}

function areEqual(prevProps: Props, nextProps: Props): boolean {
  return prevProps.poll.id === nextProps.poll.id && areEqualPoll(prevProps.poll, nextProps.poll)
}

const AccountPollListItem: React.FC<Props> = ({ poll }) => {
  const navigation = useNavigation()

  const content = () => {
    if (!poll.image || !poll.image.length) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={styles.placeholder} />
          <AppTextBold style={styles.title}>
            { poll.title }
          </AppTextBold>
        </View>
      )
    }

    return (
      <ImageBackground
        source={{ uri: poll.image[0] }}
        style={styles.image}
      >
        <View style={styles.placeholder} />
        <AppTextBold style={styles.title}>
          { poll.title }
        </AppTextBold>
      </ImageBackground>
    )
  }

  return (
    <TouchableOpacity
      style={styles.root}
      onPress={() => navigation.navigate('OnePoll', { pollId: poll.id })}
    >
      { content() }
    </TouchableOpacity>
  )
}

export default memo(AccountPollListItem, areEqual)
