import React, { useState, useEffect } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Alert,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  Linking,
} from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import * as ImageManipulator from 'expo-image-manipulator'
import * as IntentLauncher from 'expo-intent-launcher'
import * as Application from 'expo-application'
import * as mime from 'react-native-mime-types'
import i18n from 'i18n-js'
import { ImageType } from '../../store/modules/poll/types'
import { THEME } from '../../theme'
import { AppIcon } from '../ui/AppIcon'
import { AppText } from '../ui/AppText'
import logger from '../../lib/logger'
import { isIos } from '../../lib/platform'

interface Props {
  onPick: (image: ImageType) => void
  onRemove: (uri: string) => void
  selectedImages: string[]
}

type Permission = { access?: boolean, canAskAgain?: boolean }

interface Permissions {
  media: Permission
  camera: Permission
}

const styles = StyleSheet.create({
  wrapper: {
    marginBottom: 10,
  },
  image: {
    width: 120,
    height: 90,
    marginLeft: 10,
    borderRadius: 5,
    overflow: 'hidden',
    position: 'relative',
  },
  placeholder: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: 120,
    height: 90,
    padding: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: THEME.colors.GRAY,
    backgroundColor: THEME.colors.WHITE,
  },
  contentWrapper: {
    flex: 1,
    paddingHorizontal: 10,
  },
  selectedPlaceholder: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

function goToSettings(): void {
  if (isIos) {
    Linking.openURL('app-settings:')
  } else {
    IntentLauncher.startActivityAsync(
      IntentLauncher.ACTION_APPLICATION_DETAILS_SETTINGS,
      {
        data: `package:${(Application.applicationId || '')}`,
        flags: 32768,
      },
    )
  }
}

function displayError(message: string): void {
  Alert.alert(
    i18n.t('Error'),
    message,
    [
      {
        text: i18n.t('ToSettings'),
        onPress: goToSettings,
      },
      {
        text: 'OK',
      },
    ],
  )
}

async function askForPermission(permission: 'camera' | 'cameraRoll'): Promise<Permission> {
  const { status, canAskAgain } = await Permissions.askAsync(permission)
  if (status !== 'granted') {
    return {
      access: false,
      canAskAgain,
    }
  }
  return {
    access: true,
    canAskAgain,
  }
}

export const PhotoPicker: React.FC<Props> = ({ onPick, onRemove, selectedImages }) => {
  const [hasPermissions, setPermissions] = useState<Permissions>({ camera: {}, media: {} })

  const loadPermissions = async () => {
    const hasCameraPermissions = await askForPermission('camera')
    const hasMediaPermissions = await askForPermission('cameraRoll')

    setPermissions({
      camera: hasCameraPermissions,
      media: hasMediaPermissions,
    })
  }

  useEffect(() => {
    loadPermissions()
  }, [])

  const getImageName = (path: string): string => {
    const fileName = path.split('/').pop() || 'image'
    return fileName
  }

  const regrantPermissions = (message: string): void => {
    Alert.alert(
      i18n.t('CreatePoll'),
      message,
      [
        {
          text: i18n.t('GrantAccess'),
          onPress: () => loadPermissions(),
          style: 'default',
        },
        { text: 'OK', onPress: () => {} },
      ],
      { cancelable: false },
    )
  }

  const getImageSize = async (
    imageUrl: string,
  ): Promise<{ width: number, height: number }> => new Promise((resolve) => {
    Image.getSize(
      imageUrl,
      (width, height) => resolve({ width, height }),
      logger.error,
    )
  })

  const handleImage = async (imageUri: string): Promise<string> => {
    const { width, height } = await getImageSize(imageUri)
    const actions = []
    if (width > 1000) {
      const proportion = height / width
      actions.push({ resize: { width: 1000, height: 1000 * proportion } })
    }
    const { uri } = await ImageManipulator.manipulateAsync(
      imageUri,
      actions,
    )
    return uri
  }

  const saveImage = async (uri: string): Promise<void> => {
    const mimeType = mime.lookup(uri)
    const handledImage = await handleImage(uri)
    onPick({
      name: getImageName(handledImage),
      type: mimeType,
      uri: handledImage,
    })
  }

  const takePhoto = async () => {
    if (hasPermissions.camera.access === false) {
      if (hasPermissions.camera.canAskAgain === true) return regrantPermissions(i18n.t('NoAccessToCamera'))
      return displayError(i18n.t('NoAccessToCamera'))
    }

    try {
      const img = await ImagePicker.launchCameraAsync({
        allowsEditing: !isIos,
        aspect: [4, 4],
        quality: 1,
      })

      if (!img.cancelled) {
        saveImage(img.uri)
      }
    } catch (error) {
      logger.error(error)
    }
  }

  const pickImage = async () => {
    if (hasPermissions.media.access === false) {
      if (hasPermissions.media.canAskAgain === true) return regrantPermissions(i18n.t('NoAccessToMedia'))
      return displayError(i18n.t('NoAccessToMedia'))
    }

    try {
      const img = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 4],
        quality: 1,
      })
      if (!img.cancelled) {
        saveImage(img.uri)
      }
    } catch (error) {
      logger.error(error)
    }
  }

  const removeSelectedImage = (uri: string): void => {
    onRemove(uri)
  }

  const renderSelectedImages = () => (
    selectedImages.map((image) => (
      <TouchableWithoutFeedback
        key={image}
        onPress={() => removeSelectedImage(image)}
      >
        <View style={styles.image}>
          <Image
            style={{ flex: 1 }}
            source={{ uri: image }}
          />
          <View style={styles.selectedPlaceholder}>
            <AppIcon
              name="clear"
              color="white"
              onPress={() => removeSelectedImage(image)}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    ))
  )

  return (
    <View style={styles.wrapper}>
      <ScrollView
        horizontal
      >
        <TouchableOpacity
          style={{ ...styles.placeholder, marginRight: 10 }}
          onPress={takePhoto}
        >
          <AppIcon
            name="add-a-photo"
            size={20}
          />
          <AppText style={{ fontSize: 12 }}>
            { `+ ${i18n.t('Camera')}` }
          </AppText>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.placeholder}
          onPress={pickImage}
        >
          <AppIcon
            name="wallpaper"
            size={20}
          />
          <AppText style={{ fontSize: 12 }}>
            { `+ ${i18n.t('AddMedia')}` }
          </AppText>
        </TouchableOpacity>
        { renderSelectedImages() }
      </ScrollView>
    </View>
  )
}
