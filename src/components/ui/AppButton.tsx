import React from 'react'
import {
  StyleSheet, View, TouchableOpacity, TouchableNativeFeedback, Platform,
} from 'react-native'
import { AppTextBold } from './AppTextBold'
import { THEME } from '../../theme'

interface Props {
  children: any
  backgorundColor?: string
  fontColor?: string
  onPress(): void,
  style?: Record<string, any>
  rootStyle?: Record<string, any>
}

const styles = StyleSheet.create({
  root: {
    width: '100%',
    alignItems: 'center',
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    textAlign: 'center',
  },
})

export const AppButton: React.FC<Props> = (
  {
    children,
    onPress,
    backgorundColor = THEME.MAIN_COLOR,
    fontColor = THEME.colors.WHITE,
    style = {},
    rootStyle = {},
  },
) => {
  const Wrapper = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity

  return (
    <Wrapper
      onPress={onPress}
      style={{ ...styles.root, ...rootStyle }}
    >
      <View style={{ ...styles.button, backgroundColor: backgorundColor, ...style }}>
        <AppTextBold style={{ ...styles.text, color: fontColor }}>
          { children }
        </AppTextBold>
      </View>
    </Wrapper>
  )
}
