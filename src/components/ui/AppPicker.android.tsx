import React, { useState } from 'react'
import { View, StyleSheet, Picker } from 'react-native'
import { AppTextBold } from './AppTextBold'
import { THEME } from '../../theme'

type SelectOption = {
  label: string,
  value: string
}

interface Props {
  options: SelectOption[]
  onChangeValue: (value: any, index: number) => void
  style?: Record<string, any>
  btnText?: string
  error?: string
}

const styles = StyleSheet.create({
  pickerWrapper: {
    marginVertical: 10,
  },
  picker: {
    height: 50,
    width: 150,
  },
  error: {
    color: THEME.DANGER_COLOR,
    textAlign: 'center',
    marginBottom: 5,
    marginTop: 5,
  },
})

export const AppPicker: React.FC<Props> = ({
  options = [],
  onChangeValue,
  style = {},
  error = '',
}) => {
  const [pickerValue, setValue] = useState('')

  const changeValue = (value: any, index: number): void => {
    setValue(value)
    onChangeValue(value, index)
  }

  const renderOptionList = () => (options.map((option) => (
    <Picker.Item label={option.label} value={option.value} key={option.value} />
  )))

  return (
    <View style={styles.pickerWrapper}>
      <Picker
        selectedValue={pickerValue}
        style={{ ...styles.picker, ...style }}
        onValueChange={changeValue}
      >
        { renderOptionList() }
      </Picker>
      <AppTextBold style={styles.error}>
        { error }
      </AppTextBold>
    </View>
  )
}
