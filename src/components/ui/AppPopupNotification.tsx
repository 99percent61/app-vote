import React, { useEffect, useRef } from 'react'
import {
  StyleSheet,
  Dimensions,
  Animated,
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../store'
import { AppText } from './AppText'
import { THEME } from '../../theme'
import { NotificationSettings } from '../../store/modules/ui/types'
import { toggleNotification } from '../../store/modules/ui/actions'
import { AppIcon } from './AppIcon'

const deviceWidth = Dimensions.get('window').width

interface Props {}

const styles = StyleSheet.create({
  root: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '80%',
    bottom: 60,
    left: (deviceWidth * 0.2) / 2,
    padding: 10,
    borderRadius: 20,
    backgroundColor: THEME.colors.WHITE,
    shadowColor: THEME.colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 15,
  },
  text: {
    textAlign: 'center',
    paddingHorizontal: 5,
  },
})

export const AppPopupNotification: React.FC<Props> = () => {
  const {
    isVisible,
    message,
  } = useSelector<RootState, NotificationSettings>((state) => state.ui.popupNotification)
  const dispatch = useDispatch()
  const fadeAnim = useRef(new Animated.Value(0)).current
  let timerId = 0

  const animate = ({ opacity, callback }: { opacity: number, callback?: () => void }) => {
    Animated.timing(
      fadeAnim,
      {
        toValue: opacity,
        duration: 1000,
        useNativeDriver: true,
      },
    ).start(callback)
  }

  const hideNotification = (): void => {
    clearTimeout(timerId)
    animate({
      opacity: 0,
      callback: () => {
        dispatch(toggleNotification({
          isVisible: false,
          message: '',
        }))
      },
    })
  }

  useEffect(() => {
    if (isVisible) {
      animate({ opacity: 1 })
      timerId = setTimeout(() => {
        hideNotification()
      }, 5000)
    }
  }, [isVisible, fadeAnim])

  return isVisible
    ? (
      <Animated.View style={{ ...styles.root, opacity: fadeAnim }}>
        <AppText style={styles.text}>
          { message }
        </AppText>
        <AppIcon
          name="close"
          color={THEME.colors.BLUE}
          onPress={hideNotification}
        />
      </Animated.View>
    )
    : null
}
