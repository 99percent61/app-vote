import React from 'react'
import { TouchableOpacity } from 'react-native'
import { AppText } from './AppText'

interface Props {
  children: any
  style?: Record<string, any>
  onPress?: () => void
}

export const AppListItem: React.FC<Props> = ({ children, style, onPress = () => {} }) => (
  <TouchableOpacity onPress={onPress}>
    <AppText style={style}>
      { children }
    </AppText>
  </TouchableOpacity>
)
