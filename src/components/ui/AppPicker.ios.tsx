import React, { useState } from 'react'
import {
  View, StyleSheet, Picker, TouchableHighlight,
} from 'react-native'
import { AppText } from './AppText'
import { AppModal } from './AppModal'
import { AppTextBold } from './AppTextBold'
import { THEME } from '../../theme'

type SelectOption = {
  label: string,
  value: string
}

interface Props {
  options: SelectOption[]
  onChangeValue: (value: any, index: number) => void
  style?: Record<string, any>
  btnText?: string
  error?: string
}

const styles = StyleSheet.create({
  pickerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker: {
    height: 50,
    width: 200,
  },
  error: {
    color: THEME.DANGER_COLOR,
    textAlign: 'center',
    marginBottom: 5,
    marginTop: 5,
  },
})

export const AppPicker: React.FC<Props> = ({
  options = [],
  onChangeValue,
  style = {},
  btnText = '',
  error = '',
}) => {
  const [isVisibleModal, setModalVisibility] = useState(false)
  const [pickerValue, setValue] = useState('')

  const changeValue = (value: any, index: number): void => {
    setValue(value)
    onChangeValue(value, index)
  }

  const renderOptionList = () => (
    options.map((option) => (
      <Picker.Item label={option.label} value={option.value} key={option.value} />
    ))
  )

  return (
    <View>
      <TouchableHighlight onPress={() => setModalVisibility(!isVisibleModal)}>
        <AppText>
          { btnText }
        </AppText>
      </TouchableHighlight>
      <AppModal
        isVisible={isVisibleModal}
        setVisibility={setModalVisibility}
      >
        <View style={styles.pickerWrapper}>
          <Picker
            selectedValue={pickerValue}
            style={{ ...styles.picker, ...style }}
            onValueChange={changeValue}
          >
            { renderOptionList() }
          </Picker>
        </View>
      </AppModal>
      <AppTextBold style={styles.error}>
        { error }
      </AppTextBold>
    </View>
  )
}
