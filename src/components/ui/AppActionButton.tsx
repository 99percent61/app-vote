import React from 'react'
import ActionButton from 'react-native-action-button'

interface Props {
  onPress: () => void,
  btnColor?: string,
  position?: string
}

export const AppActionButton: React.FC<Props> = ({
  onPress,
  btnColor = 'rgba(231,76,60,1)',
  position = 'right',
}) => (
  <ActionButton
    buttonColor={btnColor}
    onPress={onPress}
    position={position}
  />
)
