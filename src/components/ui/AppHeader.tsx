import React from 'react'
import { View, StyleSheet, Platform } from 'react-native'
import { AppTextBold } from './AppTextBold'
import { THEME } from '../../theme'

interface Props {
  title: string
}

const isIos = Platform.OS === 'ios'
const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {
    color: isIos ? THEME.colors.BLACK : THEME.colors.WHITE,
    fontSize: 20,
  },
})

export const AppHeader: React.FC<Props> = ({ title }) => (
  <View style={styles.root}>
    <AppTextBold style={styles.title}>
      { title }
    </AppTextBold>
  </View>
)
