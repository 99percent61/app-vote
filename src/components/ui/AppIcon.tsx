import React from 'react'
import { TouchableOpacity } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'

interface Props {
  name: string
  size?: number
  color?: string,
  style?: Record<string, any>
  onPress?: () => void
}

export const AppIcon: React.FC<Props> = ({
  name,
  size = 22,
  color = 'black',
  style = {},
  onPress,
}) => {
  if (typeof onPress === 'function') {
    return (
      <TouchableOpacity onPress={onPress}>
        <MaterialIcons
          name={name}
          size={size}
          color={color}
          style={style}
          onPress={onPress}
        />
      </TouchableOpacity>
    )
  }

  return (
    <MaterialIcons
      name={name}
      size={size}
      color={color}
      style={style}
    />
  )
}
