import React from 'react'
import { StyleSheet, View } from 'react-native'
import { ProgressBar } from 'react-native-paper'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { THEME } from '../../theme'
import { AppText } from './AppText'
import { Answer } from '../../store/modules/poll/types'

interface Props {
  progress: number
  answer: Answer
  answerStyles?: Record<string, any>
  showVotesCount?: boolean
  onPress?: () => void
}

const styles = StyleSheet.create({
  answer: {
    flex: 1,
    position: 'relative',
    width: '100%',
    height: 50,
    borderWidth: 1,
    borderColor: THEME.colors.GRAY,
    borderRadius: 5,
    marginBottom: 10,
  },
  progressLine: {
    width: '100%',
    height: '100%',
    borderRadius: 5,
  },
  answerTitleWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    paddingHorizontal: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  answerTitle: {
    color: THEME.colors.WHITE,
  },
})

export const AppProgressBar: React.FC<Props> = (
  {
    progress,
    answer,
    answerStyles,
    showVotesCount,
    onPress = () => { },
  },
) => (
  <TouchableOpacity
    onPress={onPress}
  >
    <View
      style={styles.answer}
    >
      <ProgressBar
        focusable
        progress={progress}
        color={THEME.MAIN_COLOR}
        style={styles.progressLine}
      />
      <View style={styles.answerTitleWrapper}>
        <AppText style={{ ...styles.answerTitle, ...answerStyles }}>
          {answer.title}
        </AppText>
        <View style={{ flexDirection: 'row' }}>
          <AppText style={{ color: THEME.colors.WHITE }}>
            {`${Math.floor((progress * 100) * 100) / 100} %`}
          </AppText>
          {showVotesCount === true && (
            <AppText style={{ color: THEME.colors.WHITE }}>
              { ` / ${answer.votes}`}
            </AppText>
          )}
        </View>
      </View>
    </View>
  </TouchableOpacity>
)
