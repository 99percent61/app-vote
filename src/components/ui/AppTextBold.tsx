import React from 'react'
import { Text, StyleSheet } from 'react-native'

interface Props {
  style?: Record<string, any>
  children: any
}

const styles = StyleSheet.create({
  default: {
    fontFamily: 'nunito-bold',
    fontSize: 16,
  },
})

export const AppTextBold: React.FC<Props> = ({ style = {}, children = '' }) => (
  <Text style={{ ...styles.default, ...style }}>
    { children }
  </Text>
)
