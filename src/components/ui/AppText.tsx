import React from 'react'
import { Text, StyleSheet } from 'react-native'

interface Props {
  style?: Record<string, any>
  children: any
}

const styles = StyleSheet.create({
  default: {
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
})

export const AppText: React.FC<Props> = ({ style = {}, children = '' }) => (
  <Text style={{ ...styles.default, ...style }}>
    { children }
  </Text>
)
