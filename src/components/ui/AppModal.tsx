import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'
import Modal from 'react-native-modal'
import { AppIcon } from './AppIcon'
import { THEME } from '../../theme'

interface Props {
  children: any
  isVisible: boolean
  setVisibility: (value: boolean) => void
  contentStyle?: Record<string, any>
  modalHeight?: string
}

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
    padding: 20,
  },
  content: {
    backgroundColor: THEME.colors.WHITE,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  back: {
    paddingVertical: 20,
    textAlign: 'center',
  },
})

export const AppModal: React.FC<Props> = (
  {
    children,
    isVisible = false,
    setVisibility,
    contentStyle = {},
    modalHeight = '70%',
  },
) => {
  const toggleModal = (): void => {
    setVisibility(!isVisible)
  }

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={toggleModal}
      onBackButtonPress={toggleModal}
      style={styles.modal}
    >
      <View style={{ ...styles.content, height: modalHeight }}>
        <View style={{ ...styles.wrapper, ...contentStyle }}>
          { children }
        </View>
        <AppIcon
          name="highlight-off"
          size={30}
          onPress={toggleModal}
          style={styles.back}
        />
      </View>
    </Modal>
  )
}
