import React, { useState, useEffect, useRef } from 'react'
import {
  View,
  StyleSheet,
  TextInput,
  NativeSyntheticEvent,
  TextInputEndEditingEventData,
  TouchableWithoutFeedback,
} from 'react-native'
import { AppTextBold } from './AppTextBold'
import { AppText } from './AppText'
import { AppIcon } from './AppIcon'
import { THEME } from '../../theme'

interface Props {
  style?: Record<string, any>
  value: string
  onChange?: (event: any) => void
  onChangeText: (text: string) => void
  onBlur?: (event: NativeSyntheticEvent<TextInputEndEditingEventData>) => void
  onPressIcon?: () => void
  title?: string
  icon?: string
  iconSize?: number
  iconColor?: string
  withIconEye?: boolean
  placeholder?: string
  autoCorrect?: boolean
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters',
  error?: string | undefined
  textContentType?: any
  secureTextEntry?: boolean
  keyboardType?: any
  multiline?: boolean
  numberOfLines?: number
}

const styles = StyleSheet.create({
  root: {},
  inputWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#999999',
    maxHeight: 50,
    paddingVertical: 10,
  },
  input: {
    fontFamily: 'nunito-regular',
  },
  width80: {
    width: '80%',
  },
  width90: {
    width: '90%',
  },
  width100: {
    width: '100%',
  },
  title: {
    color: '#999999',
    fontSize: 12,
  },
  icon: {
    paddingRight: 10,
  },
  iconPassword: {
    paddingLeft: 10,
  },
})

export const AppInput: React.FC<Props> = (
  {
    onChangeText,
    onBlur,
    onChange,
    onPressIcon,
    value,
    placeholder = '',
    title = '',
    icon = '',
    iconSize = 22,
    iconColor = 'black',
    withIconEye = false,
    autoCorrect = false,
    autoCapitalize = 'none',
    error = '',
    style = {},
    textContentType = 'none',
    secureTextEntry = false,
    keyboardType = 'default',
    multiline = false,
    numberOfLines = 1,
  },
) => {
  const [securityText, setSecurityText] = useState<boolean>(secureTextEntry)
  const inputRef = useRef(null)

  const setFont = () => inputRef?.current.setNativeProps({
    style: { fontFamily: 'nunito-regular' },
  })

  useEffect(() => {
    setFont()
  }, [])

  const toggleSecurityText = () => {
    setSecurityText(!securityText)
  }

  const iconBlock = () => {
    if (!icon) return null

    return (
      <AppIcon
        name={icon}
        size={iconSize}
        color={iconColor}
        onPress={onPressIcon}
        style={styles.icon}
      />
    )
  }

  const iconPasswordBlock = () => {
    if (!withIconEye) return null

    return (
      <AppIcon
        name={securityText ? 'visibility-off' : 'visibility'}
        size={26}
        color={THEME.colors.BLACK}
        onPress={toggleSecurityText}
        style={styles.iconPassword}
      />
    )
  }

  const inputStyles = () => {
    const localStyles = styles.input
    const additionalStyles = (() => {
      if (icon && withIconEye) {
        return styles.width80
      }
      if (withIconEye || icon) {
        return styles.width90
      }
      return styles.width100
    })()
    return {
      ...localStyles,
      ...additionalStyles,
    }
  }

  const focusOnInput = () => inputRef?.current.focus()

  return (
    <TouchableWithoutFeedback onPress={focusOnInput}>
      <View style={styles.root}>
        <View>
          { Boolean(title) && (<AppText style={styles.title}>{ title }</AppText>) }
          <View style={{ ...styles.inputWrapper, ...style }}>
            { iconBlock() }
            <TextInput
              ref={inputRef}
              value={value}
              onChange={onChange}
              onChangeText={onChangeText}
              onEndEditing={onBlur}
              placeholder={placeholder}
              autoCorrect={autoCorrect}
              autoCapitalize={autoCapitalize}
              textContentType={textContentType}
              secureTextEntry={securityText}
              keyboardType={keyboardType}
              multiline={multiline}
              numberOfLines={numberOfLines}
              style={inputStyles()}
            />
            { iconPasswordBlock() }
          </View>
        </View>
        <AppTextBold style={THEME.text.error}>
          { error }
        </AppTextBold>
      </View>
    </TouchableWithoutFeedback>
  )
}
