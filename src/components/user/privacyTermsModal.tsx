import React, { useState } from 'react'
import {
  View,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native'
import { useDispatch } from 'react-redux'
import * as Linking from 'expo-linking'
import i18n from 'i18n-js'
import { THEME } from '../../theme'
import { AppModal } from '../ui/AppModal'
import { AppTextBold } from '../ui/AppTextBold'
import { AppText } from '../ui/AppText'
import { AppButton } from '../ui/AppButton'
import { acceptTerms } from '../../store/modules/user/actions'

const styles = StyleSheet.create({
  modalWrapper: {
    justifyContent: 'center',
  },
  modalTitle: {
    alignItems: 'center',
  },
  acceptTerms: {
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    alignItems: 'flex-end',
  },
  link: {
    color: THEME.colors.BLUE,
    paddingRight: 20,
  },
  linkWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    flex: 1,
  },
  modalButtons: {
    paddingTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    height: 50,
    width: 150,
    marginRight: 15,
  },
})

interface Props {
  isVisible: boolean
  setVisibility: React.Dispatch<React.SetStateAction<boolean>>
  afterAccept?: () => void
}

const PrivacyTermsModal: React.FC<Props> = ({ isVisible, setVisibility, afterAccept }) => {
  const [inProgressAccepting, setProgressAccepting] = useState<boolean>(false)
  const dispatch = useDispatch()
  const openPrivacyPolicy = () => {
    Linking.openURL('https://affect.flycricket.io/privacy.html')
  }

  const openTerms = () => {
    Linking.openURL('https://affect.flycricket.io/terms.html')
  }

  const acceptPolicyAndTerms = async () => {
    setProgressAccepting(true)
    await dispatch<any>(acceptTerms())
    setProgressAccepting(false)
    setVisibility(false)
    if (typeof afterAccept === 'function') {
      afterAccept()
    }
  }

  return (
    <AppModal
      isVisible={isVisible}
      setVisibility={setVisibility}
      contentStyle={styles.modalWrapper}
      modalHeight="50%"
    >
      <View style={styles.modalTitle}>
        <AppTextBold style={{ fontSize: 20 }}>
          { i18n.t('PrivacyPolicyAndTerms') }
        </AppTextBold>
      </View>
      <View style={styles.acceptTerms}>
        <AppText style={{ fontSize: 18 }}>
          { i18n.t('AcceptTerms') }
        </AppText>
      </View>
      <View style={styles.linkWrapper}>
        <TouchableOpacity
          onPress={openPrivacyPolicy}
        >
          <AppText style={styles.link}>
            { i18n.t('PrivacyPolicy') }
          </AppText>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={openTerms}
        >
          <AppText style={styles.link}>
            { i18n.t('Terms') }
          </AppText>
        </TouchableOpacity>
      </View>
      <View style={styles.modalButtons}>
        <AppButton
          onPress={() => setVisibility(false)}
          backgorundColor={THEME.colors.WHITE}
          fontColor={THEME.colors.BLACK}
          rootStyle={styles.button}
        >
          { i18n.t('Cancel') }
        </AppButton>
        <View>
          {
            inProgressAccepting
              ? (<ActivityIndicator size="large" color={THEME.colors.BLUE} />)
              : (
                <AppButton
                  onPress={acceptPolicyAndTerms}
                  rootStyle={styles.button}
                >
                  { i18n.t('Accept') }
                </AppButton>
              )
          }
        </View>
      </View>
    </AppModal>
  )
}

export default PrivacyTermsModal
