import React from 'react'
import { StyleSheet, View } from 'react-native'
import { AppIcon } from '../ui/AppIcon'
import { THEME } from '../../theme'

interface Props {
  onDelete: () => void
}

const styles = StyleSheet.create({
  root: {
    marginRight: 10,
  },
})

export const DeleteIcon: React.FC<Props> = ({ onDelete }) => (
  <View style={styles.root}>
    <AppIcon
      name="delete"
      color={THEME.colors.BLACK}
      onPress={onDelete}
    />
  </View>
)
