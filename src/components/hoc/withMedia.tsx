import React, { useState } from 'react'
import { Alert, Image } from 'react-native'
import i18n from 'i18n-js'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import * as ImageManipulator from 'expo-image-manipulator'
import * as mime from 'react-native-mime-types'

import logger from '../../lib/logger'
import { ImageType } from '../../store/modules/poll/types'

type Permission = { access?: boolean, canAskAgain?: boolean }
interface Permissions {
  media: Permission
  camera: Permission
}

export const withMedia = (WrappedComponent: any) => {
  return () => {
    const [hasPermissions, setPermissions] = useState<Permissions>({ camera: {}, media: {} })

    const displayError = (message: string): void => {
      Alert.alert(i18n.t('Error'), message)
    }

    const askForPermission = async (permission: 'camera' | 'cameraRoll'): Promise<Permission> => {
      const { status, canAskAgain } = await Permissions.askAsync(permission)
      if (status !== 'granted') {
        return {
          access: false,
          canAskAgain,
        }
      }
      return {
        access: true,
        canAskAgain,
      }
    }

    const loadPermissions = async () => {
      const hasCameraPermissions = await askForPermission('camera')
      const hasMediaPermissions = await askForPermission('cameraRoll')

      setPermissions({
        camera: hasCameraPermissions,
        media: hasMediaPermissions,
      })
    }

    const prepareImages = (imageList: ImageType[]): FormData => {
      const preparedImages = new FormData()
      imageList.forEach((image) => {
        preparedImages.append('photo', image)
      })
      return preparedImages
    }

    const regrantPermissions = (message: string): void => {
      Alert.alert(
        i18n.t('CreatePoll'),
        message,
        [
          {
            text: i18n.t('GrantAccess'),
            onPress: () => loadPermissions(),
            style: 'default',
          },
          { text: 'OK', onPress: () => {} },
        ],
        { cancelable: false },
      )
    }

    const getImageSize = async (
      imageUrl: string,
    ): Promise<{ width: number, height: number }> => new Promise((resolve) => {
      Image.getSize(
        imageUrl,
        (width, height) => resolve({ width, height }),
        logger.error,
      )
    })

    const handleImage = async (imageUri: string): Promise<string> => {
      const { width, height } = await getImageSize(imageUri)
      const actions = []
      if (width > 1000) {
        const proportion = height / width
        actions.push({ resize: { width: 1000, height: 1000 * proportion } })
      }
      const { uri } = await ImageManipulator.manipulateAsync(
        imageUri,
        actions,
      )
      return uri
    }

    const getImageName = (path: string): string => {
      const fileName = path.split('/').pop() || 'image'
      return fileName
    }

    const pickImage = async (
      onSaveImage: (props: { name: string, type: string, uri: string }) => void,
    ) => {
      if (hasPermissions.media.access === false) {
        if (hasPermissions.media.canAskAgain === true) return regrantPermissions(i18n.t('NoAccessToMedia'))
        return displayError(i18n.t('NoAccessToMedia'))
      }

      try {
        const img = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 4],
          quality: 1,
        })
        if (!img.cancelled) {
          const { uri } = img
          const mimeType = mime.lookup(uri)
          const handledImage = await handleImage(uri)
          onSaveImage({
            name: getImageName(handledImage),
            type: mimeType,
            uri: handledImage,
          })
        }
      } catch (error) {
        logger.error(error)
      }
    }

    const props = {
      pickImage,
      loadPermissions,
      displayError,
      prepareImages,
    }

    return (
      <WrappedComponent {...props} />
    )
  }
}
