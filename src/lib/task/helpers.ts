import { Task } from './types'

function isSuccessStatus(status: number): boolean {
  return status >= 200 && status < 300
}

function processUrl(url: string): string {
  if (url.startsWith('/')) {
    return url.slice(1)
  }
  return url
}

async function processResponse(response: Response): Promise<Task> {
  const jsonResponse = await response.json()
  const { status } = response
  const token: string | null = response.headers.get('x-auth-token')
  return {
    code: status,
    result: jsonResponse,
    token,
  }
}

export {
  processUrl,
  processResponse,
  isSuccessStatus,
}
