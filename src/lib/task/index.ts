import store from '../../store'
import config from '../../config'
import { Task } from './types'
import { processUrl, processResponse } from './helpers'
import logger from '../logger'

const API_URL = config.api.url
const FILES_API_URL = config.filesApi.url

export default async function (
  {
    url,
    method = 'GET',
    body,
  }: { url: string; method?: 'GET' | 'POST' | 'PUT' | 'DELETE'; body?: any },
): Promise<Task> {
  const targetUrl = `${API_URL}/${processUrl(url)}`
  const { token } = store.getState().user

  try {
    const response = await fetch(targetUrl, {
      method,
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token,
      },
      body: method !== 'GET' && body ? JSON.stringify(body) : null,
    })

    return processResponse(response)
  } catch (error) {
    logger.error(error)
    return {
      code: 500,
      result: [error.message || error, null],
      token: null,
    }
  }
}

export async function FileTask(
  { url, method = 'POST', files }: { url: string, method: 'POST' | 'PUT', files: any },
): Promise<Task> {
  const targetUrl = `${FILES_API_URL}/${processUrl(url)}`
  const { token } = store.getState().user

  try {
    const response = await fetch(targetUrl, {
      method,
      headers: {
        'x-access-token': token,
      },
      body: files,
    })
    return processResponse(response)
  } catch (error) {
    logger.error(error)
    return {
      code: 500,
      result: [error, null],
      token: null,
    }
  }
}
