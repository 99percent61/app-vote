import { AsyncStorage } from 'react-native'
import logger from '../logger'

class Storage {
  private instance = AsyncStorage

  public setItem = async ({ key, value }: { key: string, value: any }): Promise<void> => {
    try {
      await this.instance.setItem(key, value)
    } catch (error) {
      logger.error(error)
    }
  }

  public getItem = async (key: string): Promise<any> => {
    try {
      const value = await this.instance.getItem(key)
      return value
    } catch (error) {
      logger.error(error)
      return null
    }
  }

  public removeItem = async (key: string): Promise<void> => {
    try {
      await this.instance.removeItem(key)
    } catch (error) {
      logger.error(error)
    }
  }
}

export default new Storage()
