export default {
  log(...args: (string | null)[]): void {
    console.log(...args)
  },
  error(...args: (string | null)[]): void {
    console.error(...args)
  },
  debug(...args: (string | null)[]): void {
    console.debug(...args)
  },
}
