import React, { useEffect, useState } from 'react'
import { Alert } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import i18n from 'i18n-js'
import { AuthNavigator } from './AuthNavigation'
import { MainNavigation } from './MainNavigation'
import { RootState } from '../store'
import { loadToken, loadUser, logout } from '../store/modules/user/actions'
import { User } from '../store/modules/user/types'
import PrivacyTermsModal from '../components/user/privacyTermsModal'

export const AppNavigation = () => {
  const [isVisibleModal, setVisibilityModal] = useState<boolean>(false)
  const token = useSelector<RootState, string>((state) => state.user.token)
  const dispatch = useDispatch()

  const displayError = (message: string): void => {
    Alert.alert(
      i18n.t('User'),
      message,
    )
  }

  const loadUserInfo = async () => {
    const localToken: string = await dispatch<any>(loadToken())
    if (localToken) {
      const user: User = await dispatch<any>(loadUser())

      if (!user) {
        dispatch(logout())
        displayError(i18n.t('UserRemoved'))
      } else if (user.is_blocked) {
        dispatch(logout())
        displayError(i18n.t('UserIsBlocked'))
      } else if (!user.terms) {
        setVisibilityModal(true)
      }
    }
  }

  useEffect(() => {
    loadUserInfo()
  }, [token])

  return (
    <NavigationContainer>
      { token ? <MainNavigation /> : <AuthNavigator /> }
      <PrivacyTermsModal
        isVisible={isVisibleModal}
        setVisibility={setVisibilityModal}
      />
    </NavigationContainer>
  )
}
