import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import i18n from 'i18n-js'
import { AllPolls } from '../screens/AllPols'
// import { SubscribePolls } from '../screens/SubscribePolls'
import { CreatePoll } from '../screens/CreatePoll'
import { Account } from '../screens/Account'
import { AccountSettingList } from '../screens/AccountSettingList'
import { THEME } from '../theme'
import { AppIcon } from '../components/ui/AppIcon'
import { AccountLogo } from '../components/account/Logo'
import { BottomLogo } from '../components/account/BottomLogo'
import { OnePoll } from '../screens/OnePoll'
import { AccountChooseAvatar } from '../screens/AccountChooseAvatar'
import { withMedia } from '../components/hoc/withMedia'
import { AccountProfile } from '../screens/AccountProfile'
import { AccountChangePassword } from '../screens/AccountChangePassword'
import { Feedback } from '../screens/Feedback'
import { AnswerStatistics } from '../screens/AnswerStatistics'
// import { isIos } from '../lib/platform'

const ChooseAvatarWithMedia = withMedia(AccountChooseAvatar)

const navigatorOptions = {
  headerTitle: 'Vote!',
  headerStyle: {
    // backgroundColor: isIos ? THEME.colors.WHITE : THEME.MAIN_COLOR,
  },
  // headerTintColor: isIos ? THEME.MAIN_COLOR : THEME.colors.WHITE,
  headerMode: 'screen',
  cardStyle: {
    // backgroundColor: '#FFFFFF',
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
  },
}
const Tab = createBottomTabNavigator()
const WorldStack = createStackNavigator()
// const SubscriptionsStack = createStackNavigator()
const AccountStack = createStackNavigator()
const CreatePollStack = createStackNavigator()

const WorldNavigator = () => (
  <WorldStack.Navigator
    initialRouteName="AllPolls"
    screenOptions={navigatorOptions}
  >
    <WorldStack.Screen
      name="AllPolls"
      component={AllPolls}
      initialParams={{ updatePollList: false }}
      options={{
        title: i18n.t('World'),
      }}
    />
  </WorldStack.Navigator>
)

const CreatePollNavigator = () => (
  <CreatePollStack.Navigator
    initialRouteName="CreatePoll"
    screenOptions={navigatorOptions}
  >
    <CreatePollStack.Screen
      name="CreatePoll"
      component={CreatePoll}
      options={{
        title: i18n.t('CreatePoll'),
        headerTitle: i18n.t('CreatePoll'),
      }}
    />
  </CreatePollStack.Navigator>
)

// const SubscriptionsNavigator = () => (
//   <SubscriptionsStack.Navigator
//     initialRouteName="SubscribtionPolls"
//     screenOptions={navigatorOptions}
//   >
//     <SubscriptionsStack.Screen
//       name="SubscribtionPolls"
//       component={SubscribePolls}
//       options={{
//         headerTitle: i18n.t('Subscribtions'),
//       }}
//     />
//   </SubscriptionsStack.Navigator>
// )

const AccountNavigator = () => (
  <AccountStack.Navigator
    initialRouteName="Account"
    screenOptions={navigatorOptions}
  >
    <AccountStack.Screen
      name="Account"
      component={Account}
      initialParams={{ updatePollList: false }}
      options={({ navigation, route }) => ({
        title: i18n.t('Account'),
        headerTitle: (props) => <AccountLogo />,
      })}
    />
    <AccountStack.Screen
      name="AccountSettingList"
      component={AccountSettingList}
      options={{
        title: i18n.t('Settings'),
        headerTitle: i18n.t('Settings'),
      }}
    />
    <AccountStack.Screen
      name="AccountChooseAvatar"
      component={ChooseAvatarWithMedia}
      options={{
        title: i18n.t('Avatar'),
        headerTitle: i18n.t('Avatar'),
      }}
    />
    <AccountStack.Screen
      name="OnePoll"
      component={OnePoll}
      options={{
        title: i18n.t('Poll'),
        headerTitle: i18n.t('Poll'),
        cardStyle: {
          backgroundColor: THEME.colors.BLACK,
        },
      }}
    />
    <AccountStack.Screen
      name="AccountProfile"
      component={AccountProfile}
      options={{
        title: i18n.t('Profile'),
        headerTitle: i18n.t('Profile'),
      }}
    />
    <AccountStack.Screen
      name="AccountChangePassword"
      component={AccountChangePassword}
      options={{
        title: i18n.t('Password'),
        headerTitle: i18n.t('Password'),
      }}
    />
    <AccountStack.Screen
      name="AccountFeedback"
      component={Feedback}
      options={{
        title: i18n.t('Support'),
        headerTitle: i18n.t('Support'),
      }}
    />
    <AccountStack.Screen
      name="AnswerStatistics"
      component={AnswerStatistics}
      options={{
        title: i18n.t('Statistics'),
        headerTitle: i18n.t('Statistics'),
      }}
    />
  </AccountStack.Navigator>
)

export const MainNavigation = () => (
  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: THEME.colors.BLUE,
      showLabel: false,
    }}
  >
    <Tab.Screen
      name="World"
      component={WorldNavigator}
      options={{
        tabBarIcon: ({ color, size }) => (
          <AppIcon name="language" color={color} size={size} />
        ),
      }}
    />
    <Tab.Screen
      name="CreatePoll"
      component={CreatePollNavigator}
      options={{
        tabBarIcon: ({ focused, color, size }) => (
          <AppIcon name="add-circle-outline" color={focused ? THEME.colors.BLUE : THEME.MAIN_COLOR} size={34} />
        ),
      }}
    />
    {/* <Tab.Screen name="Subscribtions" component={SubscriptionsNavigator} /> */}
    <Tab.Screen
      name="Account"
      component={AccountNavigator}
      options={{
        tabBarIcon: ({ focused, color, size }) => (
          <BottomLogo color={color} size={size} focused={focused} />
        ),
      }}
    />
  </Tab.Navigator>
)
