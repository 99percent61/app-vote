import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { LoginScreen } from '../screens/Login'
import { RegistrationScreen } from '../screens/Registration'
import { AuthForgotPassword } from '../screens/AuthForgotPassword'
import { AuthChangePassword } from '../screens/AuthChangePassword'
import { THEME } from '../theme'
import { Logo } from '../components/Logo'

const navigatorOptions = {
  // headerTitle: () => (<Logo />),
  headerStyle: {
    // backgroundColor: Platform.OS === 'ios' ? '#fff' : THEME.MAIN_COLOR,
  },
  // headerTintColor: Platform.OS === 'ios' ? THEME.MAIN_COLOR : '#fff',
}

const Stack = createStackNavigator()

export const AuthNavigator = () => (
  <Stack.Navigator
    initialRouteName="Login"
    screenOptions={navigatorOptions}
  >
    <Stack.Screen
      name="Login"
      component={LoginScreen}
      options={{
        headerTitle: () => (<Logo />),
      }}
    />
    <Stack.Screen
      name="Registration"
      component={RegistrationScreen}
      options={{
        headerTitle: () => (<Logo style={{ marginLeft: -60 }} />),
      }}
    />
    <Stack.Screen
      name="AuthForgotPassword"
      component={AuthForgotPassword}
      options={{
        headerTitle: () => (<Logo style={{ marginLeft: -60 }} />),
      }}
    />
    <Stack.Screen
      name="AuthChangePassword"
      component={AuthChangePassword}
      options={{
        headerTitle: () => (<Logo style={{ marginLeft: -60 }} />),
      }}
    />
  </Stack.Navigator>
)
