import { LogoutAction } from '../user/types'

export const SET_POLLS = 'SET_POLLS'
export const SET_OWN_POLLS = 'SET_OWN_POLLS'

export interface PollState {
  list: {
    items: Poll[]
    total: number
    alreadyLoaded: number
  }
  ownList: {
    items: Poll[]
    total: number
  }
  page: {
    size: number
  }
}

export interface Poll {
  id: number
  title: string
  answers: Answer[]
  image: string[]
  audio?: string
  video?: string
  time_limit?: number
  author: {
    id: number
    name: string
    login: string
    avatar: string | null
  },
  user_vote?: number | null
  is_expired?: number
}

export interface SetPollsAction {
  type: typeof SET_POLLS
  payload: {
    polls: Poll[]
    replaceState: boolean
    total: number
  }
}

export interface SetOwnPollsAction {
  type: typeof SET_OWN_POLLS
  payload: {
    polls: Poll[]
    replaceState: boolean
    total: number
  }
}

export interface QueryListParams {
  size: number
  sort: string
  subscriptions?: number
  searchAfter: number
  isExpired: string
}

export type Answer = {
  id: number
  title: string
  error?: string
  votes?: number
}

export type ImageType = {
  name: string
  uri: string
  type: string | undefined
}

export type PollTypes = 'SET_POLLS' | 'LOGOUT' | 'SET_OWN_POLLS'
export type PollActionTypes = SetPollsAction | LogoutAction | SetOwnPollsAction

export enum StatisticsType {
  GENDER = 'gender',
  AGE = 'age',
}
