import { PollState } from './types'

export const initialState: PollState = {
  list: {
    items: [],
    total: 0,
    alreadyLoaded: 0,
  },
  ownList: {
    items: [],
    total: 0,
  },
  page: {
    size: 20,
  },
}
