import {
  PollActionTypes,
  PollState,
  SET_POLLS,
  SET_OWN_POLLS,
} from './types'
import { LOGOUT } from '../user/types'
import { initialState } from './state'

export const pollReducer = (
  state = initialState,
  action: PollActionTypes,
): PollState => {
  switch (action.type) {
    case SET_POLLS:
      const { polls, replaceState, total } = action.payload
      return {
        ...state,
        list: {
          items: replaceState ? polls : [...state.list.items, ...polls],
          alreadyLoaded: replaceState ? polls.length : state.list.items.length + polls.length,
          total,
        },
      }
    case SET_OWN_POLLS:
      const ownPolls = action.payload.polls
      const needReplaceState = action.payload.replaceState
      const ownTotal = action.payload.total
      return {
        ...state,
        ownList: {
          items: needReplaceState ? ownPolls : [...state.ownList.items, ...ownPolls],
          total: ownTotal,
        },
      }
    case LOGOUT:
      return {
        ...state,
        list: {
          items: [],
          total: 0,
          alreadyLoaded: 0,
        },
      }
    default:
      return state
  }
}
