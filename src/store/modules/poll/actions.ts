import { pollController } from '../../../controllers'
import { AppThunk } from '../../index'
import { Poll, QueryListParams } from './types'
import logger from '../../../lib/logger'

export const createPoll = (
  payload: Poll,
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.createPoll(payload)
  return result
}

export const uploadImage = (
  files: any,
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.sendFile(files)
  return result
}

export const list = (
  params: QueryListParams,
  replaceState: boolean = true,
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.list(params)
  const [error, response] = result
  let polls: Poll[] = []

  if (error) {
    logger.error(error)
    return polls
  }

  if (response && response.hits && response.hits.hits) {
    polls = response.hits.hits
    dispatch({
      type: 'SET_POLLS',
      payload: {
        polls,
        replaceState,
        total: response.hits.total.value,
      },
    })
  }
  return polls
}

export const ownList = (
  params: QueryListParams,
  replaceState: boolean = true,
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.ownList(params)
  const [error, response] = result
  let polls: Poll[] = []

  if (error) {
    logger.error(error)
    return polls
  }

  if (response && response.hits && response.hits.hits) {
    polls = response.hits.hits

    dispatch({
      type: 'SET_OWN_POLLS',
      payload: {
        polls,
        replaceState,
        total: response.hits.total.value,
      },
    })
  }
  return polls
}

export const vote = (
  body: { poll_id: number, answer_id: number },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.vote(body)
  return result
}

export const deletePoll = ({ id }: { id: number }): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.delete({ id })
  return result
}

export const update = (
  { id, is_expired }: { id: number, is_expired: 1 | 0 },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.update({ id, is_expired })
  return result
}

export const ownSingle = (
  { id }: { id: number },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.ownSingleById(id)
  const [error, response] = result

  if (error) {
    logger.error(error)
    return null
  }

  if (response && response.hits && response.hits.hits) {
    const polls = response.hits.hits
    return polls.length ? polls[0] : null
  }

  return null
}

export const getStatisticsByPollId = (
  { pollId }: { pollId: number },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await pollController.statisticsByPollId({ pollId })
  return result
}
