import { AppThunk } from '../../index'

export const toggleNotification = (
  payload: { isVisible: boolean, message: string },
): AppThunk<Promise<void>> => async (dispatch) => {
  dispatch({
    type: 'TOGGLE_NOTIFICATION',
    payload,
  })
}
