import { UiState } from './types'

export const initialState: UiState = {
  popupNotification: {
    isVisible: false,
    message: '',
  },
}
