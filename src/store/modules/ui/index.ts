import {
  TOGGLE_NOTIFICATION,
  UiActionTypes,
  UiState,
} from './types'
import { initialState } from './state'

export const uiReducer = (
  state = initialState,
  action: UiActionTypes,
): UiState => {
  switch (action.type) {
    case TOGGLE_NOTIFICATION:
      return {
        ...state,
        popupNotification: action.payload,
      }
    default:
      return state
  }
}
