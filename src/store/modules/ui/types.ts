export const TOGGLE_NOTIFICATION = 'TOGGLE_NOTIFICATION'

export type NotificationSettings = {
  isVisible: boolean
  message: string
}

export interface UiState {
  popupNotification: NotificationSettings
}

export interface ShowNotificationAction {
  type: typeof TOGGLE_NOTIFICATION,
  payload: {
    isVisible: boolean
    message: string
  }
}

export type UiTypes = 'TOGGLE_NOTIFICATION'
export type UiActionTypes = ShowNotificationAction
