import { UserState } from './types'

export const defaultCurrent = {
  id: 0,
  login: '',
  email: '',
  name: '',
  avatar: null,
  black_list: [],
  settings: {},
  is_blocked: false,
  sex: '',
  age: 0,
  subscribers: [],
  subscriptions: [],
}

export const initialState: UserState = {
  current: defaultCurrent,
  token: '',
}
