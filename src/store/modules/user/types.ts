export const SET_USER = 'SET_USER'
export const SET_TOKEN = 'SET_TOKEN'
export const LOGOUT = 'LOGOUT'

export interface UserState {
  current: User
  token: string
}

export interface User {
  id: number
  login: string
  email: string
  name: string
  avatar: null | string
  black_list: number[]
  settings: {}
  is_blocked: boolean
  sex: string
  age: number
  subscribers: number[]
  subscriptions: number[]
  terms: 1 | 0
}

export interface SetUserAction {
  type: typeof SET_USER
  payload: User
}

export interface SetTokenAction {
  type: typeof SET_TOKEN,
  payload: {
    token: string
  }
}

export interface LogoutAction {
  type: typeof LOGOUT,
  payload: undefined | null
}

export interface RegistrationUser {
  login: string
  email: string
  name: string
  sex: string
  age: number
  password: string
  repeatedPassword: string
}

export type UserTypes = 'SET_USER' | 'SET_TOKEN' | 'LOGOUT'
export type UserActionTypes = SetUserAction | SetTokenAction | LogoutAction
