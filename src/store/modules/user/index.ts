import {
  UserActionTypes,
  UserState,
  SET_TOKEN,
  SET_USER,
  LOGOUT,
} from './types'
import { initialState, defaultCurrent } from './state'
import storage from '../../../lib/asyncStorage'

export const userReducer = (
  state = initialState,
  action: UserActionTypes,
): UserState => {
  switch (action.type) {
    case SET_TOKEN:
      storage.setItem({ key: 'token', value: action.payload.token })
      return {
        ...state,
        token: action.payload.token,
      }
    case SET_USER:
      storage.setItem({ key: 'user', value: JSON.stringify(action.payload) })
      return {
        ...state,
        current: action.payload,
      }
    case LOGOUT:
      storage.setItem({ key: 'token', value: '' })
      storage.setItem({ key: 'user', value: JSON.stringify({}) })
      return {
        ...state,
        current: defaultCurrent,
        token: '',
      }
    default:
      return state
  }
}
