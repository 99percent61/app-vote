import { userController } from '../../../controllers'
import { AppThunk } from '../../index'
import { RegistrationUser } from './types'
import storage from '../../../lib/asyncStorage'
import logger from '../../../lib/logger'

export const createSession = (
  payload: { login: string, password: string },
): AppThunk<Promise<number>> => async (dispatch) => {
  const { token, code } = await userController.createSession(payload)
  if (token) {
    dispatch({
      type: 'SET_TOKEN',
      payload: { token },
    })
  }
  return code
}

export const loadToken = (): AppThunk<Promise<void>> => async (dispatch) => {
  const token = await storage.getItem('token')
  if (token) {
    dispatch({
      type: 'SET_TOKEN',
      payload: { token },
    })
  }
  return token
}

export const loadUser = (): AppThunk<Promise<void>> => async (dispatch) => {
  const { result } = await userController.me()
  const [error, user] = result
  if (error) {
    logger.error(error)
    if ((error as string).includes('not found')) {
      return null
    }
    try {
      const localUser = await storage.getItem('user')
      const prepared = JSON.parse(localUser)
      return prepared
    } catch (err) {
      logger.error(err)
    }
  }
  dispatch({
    type: 'SET_USER',
    payload: user,
  })
  return user
}

export const logout = (): AppThunk<Promise<void>> => async (dispatch) => {
  dispatch({
    type: 'LOGOUT',
  })
}

export const registerUser = (
  payload: RegistrationUser,
): AppThunk<Promise<any>> => async (dispatch) => {
  const { token, result } = await userController.registerUser(payload)
  if (token) {
    dispatch({
      type: 'SET_TOKEN',
      payload: { token },
    })
  }
  return result
}

export const sendCode = (
  { email }: { email: string },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await userController.sendCodeForRepairPass({ email })
  return result
}

export const confirmCode = (
  payload: { email: string, code: string },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await userController.confirmCode(payload)
  return result
}

export const changePasswordByToken = (
  payload: { email: string, password: string, token: string, repeatedPassword: string },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await userController.changePasswordByToken(payload)
  return result
}

export const changePasswordByPass = (
  payload: { currentPassword: string, newPassword: string, repeatedPassword: string },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await userController.changePasswordByPass(payload)
  return result
}

export const update = (
  payload: { avatar: string },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await userController.update(payload)
  return result
}

export const feedback = (
  payload: { title: string, email: string, message: string },
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await userController.sendFeedback(payload)
  return result
}

export const acceptTerms = (): AppThunk<Promise<any>> => async (dispatch) => {
  const { result, code } = await userController.acceptTerms()
  if (code === 200) {
    const user = result[1]
    dispatch({
      type: 'SET_USER',
      payload: user,
    })
  }
}
