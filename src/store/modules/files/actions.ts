import { filesController } from '../../../controllers'
import { AppThunk } from '../../index'

export const uploadImages = (
  files: any,
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await filesController.uploadImages(files)
  return result
}
