import { Complaint } from '../../../controllers/complaint'
import { AppThunk } from '../../index'

export const createComplaint = (
  payload: { poll_id: number, type: string }
): AppThunk<Promise<any>> => async (dispatch) => {
  const { result } = await Complaint.createReport(payload)
  return result
}
