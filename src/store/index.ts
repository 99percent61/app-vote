import {
  createStore, combineReducers, applyMiddleware, Action,
} from 'redux'
import thunk, { ThunkMiddleware, ThunkAction } from 'redux-thunk'
import { userReducer } from './modules/user'
import { pollReducer } from './modules/poll'
import { uiReducer } from './modules/ui'
import { UserActionTypes, UserTypes } from './modules/user/types'
import { PollTypes, PollActionTypes } from './modules/poll/types'
import { UiTypes, UiActionTypes } from './modules/ui/types'

const rootReducer = combineReducers({
  user: userReducer,
  poll: pollReducer,
  ui: uiReducer,
})

export type Types = UserTypes | PollTypes | UiTypes
export type ActionTypes = UserActionTypes | PollActionTypes | UiActionTypes
export type RootState = ReturnType<typeof rootReducer>

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<Types>
>

export default createStore(
  rootReducer,
  applyMiddleware(thunk as ThunkMiddleware<RootState, ActionTypes>),
)
