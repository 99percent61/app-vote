import React, { useEffect, useState, useLayoutEffect } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { AllPollsProps } from './types'
import { list } from '../store/modules/poll/actions'
import { RootState } from '../store'
import { Poll } from '../store/modules/poll/types'
import { PollList } from '../components/polls/PollList'
import { AppIcon } from '../components/ui/AppIcon'
import { THEME } from '../theme'
import { Logo } from '../components/Logo'
import { isAndroid } from '../lib/platform'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export const AllPolls: React.FC<AllPollsProps> = ({ route, navigation }) => {
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const dispatch = useDispatch()
  const polls = useSelector<RootState, Poll[]>((state) => state.poll.list.items)
  const total = useSelector<RootState, number>((state) => state.poll.list.total)
  const pageSize = useSelector<RootState, number>((state) => state.poll.page.size)
  const alreadyLoaded = useSelector<RootState, number>((state) => state.poll.list.alreadyLoaded)

  const loadPolls = async (
    {
      searchAfter = 999999999999999,
      replaceState = true,
      forceRefresh = true,
    }: { searchAfter?: number, replaceState?: boolean, forceRefresh?: boolean } = {},
  ): Promise<void> => {
    if ((total <= pageSize && !forceRefresh) || refreshing) return
    setRefreshing(true)
    await dispatch(list({
      size: 20,
      isExpired: '0',
      sort: 'id:desc',
      subscriptions: 0,
      searchAfter,
    }, replaceState))
    setRefreshing(false)
  }

  useEffect(() => {
    loadPolls()
  }, [])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (route.params.updatePollList) {
        loadPolls()
        navigation.setParams({ updatePollList: false })
      }
    })

    return unsubscribe
  }, [navigation, route])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => <View />,
      headerTitle: () => <Logo style={{ marginRight: isAndroid ? 0 : 0 }} />,
      headerRight: () => (
        <TouchableOpacity
          onPress={() => navigation.navigate('CreatePoll')}
          style={{ marginRight: 10 }}
        >
          <AppIcon
            name="add-circle-outline"
            size={26}
            color={THEME.colors.BLUE}
          />
        </TouchableOpacity>
      ),
    })
  }, [navigation])

  const onRefresh = (): Promise<void> => (
    loadPolls()
  )

  const onEndReached = (): void => {
    const lastPoll = polls.slice().pop()
    const isTheEnd = alreadyLoaded >= total
    if (!isTheEnd && lastPoll) {
      loadPolls({ searchAfter: lastPoll.id, replaceState: false, forceRefresh: false })
    }
  }

  return (
    <View style={styles.root}>
      <PollList
        list={polls}
        refreshing={refreshing}
        onRefresh={onRefresh}
        onEndReached={onEndReached}
      />
    </View>
  )
}
