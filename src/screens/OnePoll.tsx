import React, { useLayoutEffect, useState, useEffect } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Dimensions,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native'
import { useDispatch } from 'react-redux'
import i18n from 'i18n-js'
import { OnePollProps } from './types'
import { ImageCarousel } from '../components/image/ImageCarousel'
import { AppTextBold } from '../components/ui/AppTextBold'
import { THEME } from '../theme'
import { Answer, Poll } from '../store/modules/poll/types'
import { AppText } from '../components/ui/AppText'
import { AppProgressBar } from '../components/ui/AppProgressBar'
import { AppIcon } from '../components/ui/AppIcon'
import { deletePoll, update, ownSingle } from '../store/modules/poll/actions'
import { DeleteIcon } from '../components/onePoll/DeleteIcon'
import logger from '../lib/logger'
import { toggleNotification } from '../store/modules/ui/actions'
import { AppIconAntDesign } from '../components/ui/AppIconAntDesign'

const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height
const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  imageWrapper: {
    height: deviceHeight * 0.4,
    borderRadius: 10,
    overflow: 'hidden',
  },
  contentWrapper: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: THEME.colors.WHITE,
    padding: 10,
  },
  title: {
    fontSize: 18,
  },
  author: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  avatarWrapper: {
    borderRadius: 50,
    overflow: 'hidden',
    marginRight: 5,
  },
  avatar: {
    flex: 1,
    width: 25,
    height: 25,
  },
  login: {
    color: THEME.colors.GRAY2,
    fontSize: 14,
  },
  chartDetails: {
    color: THEME.colors.BLUE,
    fontSize: 14,
  },
  buttonWrapper: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingBottom: 20,
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
  },
  shadow: {
    shadowColor: THEME.colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
  },
  stopButton: {
    backgroundColor: THEME.colors.BLUE,
  },
  removeButton: {
    backgroundColor: THEME.DANGER_COLOR,
  },
})

function displayError(message: string): void {
  Alert.alert(i18n.t('Error'), message)
}

export const OnePoll: React.FC<OnePollProps> = ({ route, navigation }) => {
  const [poll, setPoll] = useState<Poll | null>(null)
  const { pollId } = route.params
  const dispatch = useDispatch()

  const navigateToAccount = (updatePollList: boolean = true): void => (
    navigation.navigate('Account', { updatePollList })
  )

  const navigateToStatistics = (): void => {
    navigation.navigate('AnswerStatistics', { poll })
  }

  const fetchPoll = async (id: number) => {
    const result = await dispatch<any>(ownSingle({ id: pollId }))
    if (!result) {
      return toggleNotification({ isVisible: true, message: i18n.t('ErrorFetchPoll') })
    }

    return setPoll(result)
  }

  useEffect(() => {
    fetchPoll(pollId)
  }, [pollId])

  const deletePollById = async (id: number): Promise<any> => {
    const [err, result] = await dispatch<any>(deletePoll({ id }))
    if (result === true) {
      return navigateToAccount()
    }
    if (err) {
      logger.error(err)
    }
    displayError(i18n.t('DeletePollError'))
  }

  const stopPollById = async (id: number): Promise<any> => {
    const [err, result] = await dispatch<any>(update({ id, is_expired: 1 }))
    if (result === true) {
      return navigateToAccount()
    }
    if (err) {
      logger.error(err)
    }
    displayError(i18n.t('StopPollError'))
  }

  const showAlert = ({ title = '', message = '', callback }: { title: string, message: string, callback: any }): void => {
    Alert.alert(
      title,
      message,
      [
        {
          text: i18n.t('Yes'),
          onPress: callback,
          style: 'default',
        },
        {
          text: i18n.t('No'),
          onPress: () => {},
        },
      ],
      { cancelable: false },
    )
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <DeleteIcon
          onDelete={() => showAlert({
            title: i18n.t('DeletePoll'),
            message: i18n.t('DeletePoll?'),
            callback: () => deletePollById(poll.id),
          })}
        />
      ),
    })
  }, [navigation])

  if (!poll) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      </View>
    )
  }

  const getUserAvatar = () => {
    let avatar = require('../../assets/person.png')
    if (poll.author && poll.author.avatar) {
      avatar = {
        uri: poll.author.avatar,
      }
    }
    return avatar
  }

  const imageBlock = () => {
    if (!poll.image || !poll.image.length) {
      return null
    }

    return (
      <View style={styles.imageWrapper}>
        <ImageCarousel
          images={poll.image || []}
          parentWidth={deviceWidth}
          style={{ borderRadius: 10 }}
          initialSliderHeight={deviceHeight * 0.4}
          fixedHeight
        />
      </View>
    )
  }

  const calculateProgress = (answer: Answer): number => {
    const voteCount = poll.answers.reduce((acc, item) => (acc + (item.votes || 0)), 0)
    const currentVotes = answer.votes || 0
    const percents = currentVotes / (voteCount || 1)
    return percents
  }

  const renderAnswers = () => poll.answers.map((answer) => {
    const progress = calculateProgress(answer)

    return (
      <AppProgressBar
        key={answer.id}
        answer={answer}
        progress={progress}
        showVotesCount
      />
    )
  })

  return (
    <View style={styles.root}>
      { imageBlock() }
      <ScrollView
        style={{
          ...styles.contentWrapper,
          height: imageBlock() ? deviceHeight * 0.6 : deviceHeight,
        }}
      >
        <AppTextBold style={styles.title}>
          { poll.title }
        </AppTextBold>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <View style={styles.author}>
            <View style={styles.avatarWrapper}>
              <Image
                source={getUserAvatar()}
                style={styles.avatar}
              />
            </View>
            <AppText style={styles.login}>
              { poll.author.login }
            </AppText>
          </View>
          <TouchableOpacity
            style={{ flexDirection: 'row', alignItems: 'center' }}
            onPress={navigateToStatistics}
          >
            <AppIconAntDesign
              name="piechart"
              style={{ marginRight: 5 }}
              color={THEME.colors.BLUE}
            />
            <AppText style={styles.chartDetails}>
              { i18n.t('Chart') }
            </AppText>
          </TouchableOpacity>
        </View>
        { renderAnswers() }
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            style={{ ...styles.button, ...styles.removeButton, ...styles.shadow }}
            onPress={() => showAlert({
              title: i18n.t('DeletePoll'),
              message: i18n.t('DeletePoll?'),
              callback: () => deletePollById(poll.id),
            })}
          >
            <AppIcon
              name="delete"
            />
          </TouchableOpacity>
          {
            !poll.is_expired && (
              <TouchableOpacity
                style={{ ...styles.button, ...styles.stopButton, ...styles.shadow }}
                onPress={() => showAlert({
                  title: i18n.t('StopPoll'),
                  message: i18n.t('StopPoll?'),
                  callback: () => stopPollById(poll.id),
                })}
              >
                <AppIcon
                  name="done"
                />
              </TouchableOpacity>
            )
          }
        </View>
      </ScrollView>
    </View>
  )
}
