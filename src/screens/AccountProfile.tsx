import React from 'react'
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { useSelector } from 'react-redux'
import i18n from 'i18n-js'
import { RootState } from '../store'
import { User } from '../store/modules/user/types'
import { AppText } from '../components/ui/AppText'
import { THEME } from '../theme'
import { AppTextBold } from '../components/ui/AppTextBold'
import { BaseProps } from './types'

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: THEME.colors.WHITE,
  },
  title: {
    marginRight: 30,
  },
  value: {
    color: THEME.colors.BLUE,
  },
})

export const AccountProfile: React.FC<BaseProps> = ({ navigation }) => {
  const user = useSelector<RootState, User>((state) => state.user.current)

  const renderInfo = () => {
    const info = [
      {
        title: i18n.t('Login'),
        value: user.login,
        styles: {},
        action: () => {},
      },
      {
        title: i18n.t('Name'),
        value: user.name,
        styles: {},
        action: () => {},
      },
      {
        title: i18n.t('Email'),
        value: user.email,
        styles: {},
        action: () => {},
      },
      {
        title: i18n.t('Password'),
        value: i18n.t('Change'),
        styles: { fontWeight: '700' },
        action: () => navigation.navigate('AccountChangePassword'),
      },
    ]

    return info.map((el) => (
      <TouchableWithoutFeedback
        onPress={el.action}
        key={el.title}
      >
        <View style={styles.itemWrapper}>
          <AppTextBold style={styles.title}>
            { el.title }
          </AppTextBold>
          <AppText style={{ ...styles.value, ...el.styles }}>
            { el.value }
          </AppText>
        </View>
      </TouchableWithoutFeedback>
    ))
  }

  return (
    <View style={styles.root}>
      { renderInfo() }
    </View>
  )
}
