import React, { useState } from 'react'
import {
  View,
  ScrollView,
  StyleSheet,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native'
import { useDispatch } from 'react-redux'
import i18n from 'i18n-js'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { LoginProps } from './types'
import { AppTextBold } from '../components/ui/AppTextBold'
import { AppInput } from '../components/ui/AppInput'
import { AppButton } from '../components/ui/AppButton'
import { createSession, loadUser } from '../store/modules/user/actions'
import { AppText } from '../components/ui/AppText'
import { THEME } from '../theme'
import { isIos } from '../lib/platform'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 50,
  },
  loginForm: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 18,
    marginBottom: 20,
  },
  button: {
    width: '100%',
  },
})

export const LoginScreen: React.FC<LoginProps> = ({ route, navigation }) => {
  const [isInProcessLogin, setProcessLogin] = useState<boolean>(false)

  const validationSchema = Yup.object({
    login: Yup.string()
      .required(i18n.t('RequiredString')),
    password: Yup.string()
      .min(8, i18n.t('AtLeast8'))
      .max(50, i18n.t('TooLong'))
      .required(i18n.t('RequiredString')),
  })
  const dispatch = useDispatch()

  const displayError = (code: string): void => {
    const errorMap = new Map([
      ['404', i18n.t('UserNotFound')],
      ['403', i18n.t('WrongPassword')],
      ['default', i18n.t('LogInError')],
    ])
    const error = errorMap.has(code) ? errorMap.get(code) : errorMap.get('default')
    Alert.alert(
      i18n.t('Login'),
      error,
    )
  }

  const logInUser = async (values: { login: string, password: string }): Promise<void> => {
    setProcessLogin(true)
    const code = await dispatch<any>(createSession({
      login: values.login,
      password: values.password,
    }))
    if (code !== 200) {
      setProcessLogin(false)
      return displayError(String(code))
    }
    dispatch(loadUser())
  }

  const actionBlock = (
    handleSubmit: (e?: React.FormEvent<HTMLFormElement> | undefined) => void,
  ) => {
    if (isInProcessLogin) {
      return (
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      )
    }

    return (
      <AppButton
        style={styles.button}
        onPress={handleSubmit}
      >
        { i18n.t('Enter') }
      </AppButton>
    )
  }

  return (
    <KeyboardAvoidingView
      behavior={ isIos ? 'padding' : 'height' }
      style={styles.root}
    >
      <ScrollView contentContainerStyle={styles.root}>
        <Formik
          initialValues={{ login: '', password: '' }}
          onSubmit={logInUser}
          validationSchema={validationSchema}
        >
          {({
            handleChange, handleBlur, handleSubmit, values, errors, touched,
          }) => (
            <View style={styles.loginForm}>
              <AppTextBold style={styles.title}>
                { i18n.t('LogIn') }
              </AppTextBold>
              <AppInput
                onChangeText={handleChange('login')}
                onBlur={handleBlur('login')}
                value={values.login}
                error={touched.login && errors.login ? errors.login : ''}
                placeholder={i18n.t('EnterLogin')}
                textContentType="username"
              />
              <AppInput
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                value={values.password}
                error={touched.password && errors.password ? errors.password : ''}
                placeholder={i18n.t('EnterPassword')}
                textContentType="password"
                secureTextEntry
                withIconEye
              />
              { actionBlock(handleSubmit) }
            </View>
          )}
        </Formik>
        <TouchableOpacity
          onPress={() => navigation.navigate('Registration')}
        >
          <AppText style={{ marginBottom: 15 }}>
            { i18n.t('Registration') }
          </AppText>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('AuthForgotPassword')}
        >
          <AppText>
            { i18n.t('ForgotPassword?') }
          </AppText>
        </TouchableOpacity>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}
