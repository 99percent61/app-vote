import React, { useLayoutEffect } from 'react'
import { StyleSheet, Dimensions } from 'react-native'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import i18n from 'i18n-js'
import { AccountProps } from './types'
import { AccountHamburger } from '../components/account/Hamburger'
import { ActivePolls } from '../components/account/polls/ActivePolls'
import { InActivePolls } from '../components/account/polls/InActivePolls'
import { THEME } from '../theme'

const styles = StyleSheet.create({
  tabBarContainer: {
    backgroundColor: THEME.colors.WHITE,
  },
  indicator: {
    backgroundColor: THEME.colors.BLUE,
  },
})

const initialLayout = { width: Dimensions.get('window').width };

export const Account: React.FC<AccountProps> = ({ navigation, route }) => {
  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([
    { key: 'first', title: i18n.t('ActivePolls') },
    { key: 'second', title: i18n.t('CompletedPolls') },
  ])
  const renderScene = SceneMap({
    first: ActivePolls,
    second: InActivePolls,
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <AccountHamburger navigation={navigation} route={route} />
      ),
    })
  }, [navigation])

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          style={styles.tabBarContainer}
          activeColor={THEME.colors.BLACK}
          inactiveColor={THEME.colors.GRAY2}
          indicatorStyle={styles.indicator}
        />
      )}
      lazy
    />
  )
}
