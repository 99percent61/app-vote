import React, { useState } from 'react'
import {
  View,
  ScrollView,
  StyleSheet,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native'
import * as Linking from 'expo-linking'
import { useDispatch } from 'react-redux'
import { RadioButton } from 'react-native-paper'
import i18n from 'i18n-js'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { LoginProps } from './types'
import { AppTextBold } from '../components/ui/AppTextBold'
import { AppInput } from '../components/ui/AppInput'
import { AppButton } from '../components/ui/AppButton'
import { acceptTerms, registerUser } from '../store/modules/user/actions'
import { AppText } from '../components/ui/AppText'
import { THEME } from '../theme';
import { isIos } from '../lib/platform'

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  registrationForm: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 18,
    marginBottom: 20,
  },
  genderWrapper: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  genderTitle: {
    paddingTop: isIos ? 9 : 6,
  },
  genderItems: {
    paddingLeft: 15,
  },
  genderItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  link: {
    color: THEME.colors.BLUE,
    paddingRight: 5,
    paddingLeft: 5,
  },
  signUp: {
    marginTop: 10,
  },
  terms: {
    display: 'flex',
    flexDirection: 'row',
  },
})

export const RegistrationScreen: React.FC<LoginProps> = ({ route, navigation }) => {
  const [isInProcessRegistration, setProcessRegistration] = useState<boolean>(false)
  const validationSchema = Yup.object({
    login: Yup.string()
      .required(i18n.t('RequiredString')),
    password: Yup.string()
      .min(8, i18n.t('AtLeast8'))
      .max(50, i18n.t('TooLong'))
      .required(i18n.t('RequiredString')),
    repeatedPassword: Yup
      .string()
      .required(i18n.t('RequiredString'))
      .oneOf([Yup.ref('password')], i18n.t('PasswordsMatch')),
    email: Yup.string()
      .email(i18n.t('NotLikeEmail'))
      .required(i18n.t('RequiredString')),
    name: Yup.string()
      .required(i18n.t('RequiredString')),
    age: Yup.number()
      .min(3, i18n.t('MinNumberValue3'))
      .max(150, i18n.t('MaxNumberValue150'))
      .required(i18n.t('RequiredString')),
    sex: Yup.string()
      .required(i18n.t('RequiredString')),
  })
  const dispatch = useDispatch()

  const displayError = (message: string): void => {
    const errorMap = new Map([
      ['Login busy', i18n.t('LoginBusy')],
      ['Email busy', i18n.t('EmailBusy')],
      ['default', i18n.t('RegisterError')],
    ])
    const error = errorMap.has(message) ? errorMap.get(message) : errorMap.get('default')
    Alert.alert(
      i18n.t('Registration'),
      error,
    )
  }

  const register = async (values: {
    login: string,
    password: string,
    repeatedPassword: string,
    email: string,
    name: string,
    age: number,
    sex: string,
  }) => {
    setProcessRegistration(true)
    const [error] = await dispatch<any>(registerUser(values))
    if (error) {
      setProcessRegistration(false)
      return displayError(error)
    }
    await dispatch<any>(acceptTerms())
  }

  const openPrivacyPolicy = () => {
    Linking.openURL('https://affect.flycricket.io/privacy.html')
  }

  const openTerms = () => {
    Linking.openURL('https://affect.flycricket.io/terms.html')
  }

  const actionBlock = (
    handleSubmit: (e?: React.FormEvent<HTMLFormElement> | undefined) => void,
  ) => {
    if (isInProcessRegistration) {
      return (
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      )
    }

    return (
      <AppButton
        onPress={handleSubmit}
        style={styles.signUp}
      >
        { i18n.t('SignUp') }
      </AppButton>
    )
  }

  return (
    <KeyboardAvoidingView
      behavior={ isIos ? 'padding' : 'height' }
      style={styles.root}
    >
      <ScrollView>
        <Formik
          initialValues={{
            login: '', password: '', repeatedPassword: '', email: '', name: '', age: '', sex: 'male',
          }}
          onSubmit={register}
          validationSchema={validationSchema}
        >
          {({
            handleChange, handleBlur, handleSubmit, values, errors, touched,
          }) => (
            <View style={styles.registrationForm}>
              <AppTextBold style={styles.title}>
                { i18n.t('Registration') }
              </AppTextBold>
              <AppInput
                onChangeText={handleChange('login')}
                onBlur={handleBlur('login')}
                value={values.login}
                error={touched.login && errors.login ? errors.login : ''}
                placeholder={i18n.t('EnterLogin')}
                textContentType="username"
              />
              <AppInput
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                error={touched.email && errors.email ? errors.email : ''}
                placeholder={i18n.t('EnterEmail')}
                textContentType="emailAddress"
              />
              <AppInput
                onChangeText={handleChange('name')}
                onBlur={handleBlur('name')}
                value={values.name}
                error={touched.name && errors.name ? errors.name : ''}
                placeholder={i18n.t('EnterName')}
                textContentType="name"
              />
              <AppInput
                onChangeText={handleChange('age')}
                onBlur={handleBlur('age')}
                value={String(values.age)}
                error={touched.age && errors.age ? errors.age : ''}
                placeholder={i18n.t('EnterAge')}
                keyboardType="number-pad"
              />
              <View style={styles.genderWrapper}>
                <AppTextBold style={styles.genderTitle}>
                  { i18n.t('Gender') }
                </AppTextBold>
                <View style={styles.genderItems}>
                  <RadioButton.Group
                    onValueChange={(value) => handleChange('sex')(value)}
                    value={values.sex}
                  >
                    <View style={styles.genderItem}>
                      <RadioButton
                        value="male"
                        color={THEME.colors.BLUE}
                      />
                      <TouchableOpacity onPress={() => handleChange('sex')('male')}>
                        <AppText>
                          { i18n.t('Male') }
                        </AppText>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.genderItem}>
                      <RadioButton
                        value="female"
                        color={THEME.colors.BLUE}
                      />
                      <TouchableOpacity onPress={() => handleChange('sex')('female')}>
                        <AppText>
                          { i18n.t('Female') }
                        </AppText>
                      </TouchableOpacity>
                    </View>
                  </RadioButton.Group>
                </View>
              </View>
              <AppTextBold style={THEME.text.error}>
                { touched.password && errors.password ? errors.password : '' }
              </AppTextBold>
              <AppInput
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                value={values.password}
                error={touched.password && errors.password ? errors.password : ''}
                placeholder={i18n.t('EnterPassword')}
                textContentType="newPassword"
                secureTextEntry
                withIconEye
              />
              <AppInput
                onChangeText={handleChange('repeatedPassword')}
                onBlur={handleBlur('repeatedPassword')}
                value={values.repeatedPassword}
                error={touched.repeatedPassword && errors.repeatedPassword ? errors.repeatedPassword : ''}
                placeholder={i18n.t('ConfirmPassword')}
                textContentType="newPassword"
                secureTextEntry
                withIconEye
              />
              <AppText>
                { i18n.t('ClickingSignUp') }
              </AppText>
              <View style={styles.terms}>
                <TouchableOpacity
                  onPress={openPrivacyPolicy}
                >
                  <AppText style={styles.link}>
                    { i18n.t('PrivacyPolicy') }
                  </AppText>
                </TouchableOpacity>
                <AppText>and</AppText>
                <TouchableOpacity
                  onPress={openTerms}
                >
                  <AppText style={styles.link}>
                    { i18n.t('Terms') }
                  </AppText>
                </TouchableOpacity>
              </View>
              { actionBlock(handleSubmit) }
            </View>
          )}
        </Formik>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}
