import React from 'react'
import { View, StyleSheet } from 'react-native'
import i18n from 'i18n-js'
import { AuthProps } from './types'
import { AppButton } from '../components/ui/AppButton'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    marginBottom: 10,
  },
})

export const AuthScreen: React.FC<AuthProps> = ({ route, navigation }) => (
  <View style={styles.root}>
    <AppButton
      style={styles.button}
      onPress={() => navigation.navigate('Login')}
    >
      { i18n.t('Login') }
    </AppButton>
    <AppButton
      style={styles.button}
      onPress={() => navigation.navigate('Registration')}
    >
      { i18n.t('Registration') }
    </AppButton>
  </View>
)
