import React, { useState } from 'react'
import {
  View, StyleSheet, Alert, TouchableOpacity, ActivityIndicator,
} from 'react-native'
import { useDispatch } from 'react-redux'
import i18n from 'i18n-js'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { AppTextBold } from '../components/ui/AppTextBold'
import { AppInput } from '../components/ui/AppInput'
import { AppButton } from '../components/ui/AppButton'
import { sendCode, confirmCode } from '../store/modules/user/actions'
import { toggleNotification } from '../store/modules/ui/actions'
import { AppText } from '../components/ui/AppText'
import { AuthForgotPasswordProps } from './types'
import { THEME } from '../theme'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingBottom: 50,
  },
  formWrapper: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  title: {
    fontSize: 18,
    marginBottom: 20,
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export const AuthForgotPassword: React.FC<AuthForgotPasswordProps> = ({ navigation }) => {
  const [isSentCode, setSentCode] = useState(false)
  const [isProcessingRequest, setIsProcessingRequest] = useState(false)
  const dispatch = useDispatch()
  const validationSchema = Yup.object({
    email: Yup.string()
      .email(i18n.t('NotLikeEmail'))
      .required(i18n.t('RequiredString')),
  })
  const validationSchemaWithCode = Yup.object({
    email: Yup.string()
      .email(i18n.t('NotLikeEmail'))
      .required(i18n.t('RequiredString')),
    code: Yup.number()
      .required(i18n.t('RequiredString')),
  })

  const displayError = (message: string): void => {
    const errorMap = new Map([
      ['email must be a valid email', i18n.t('NotLikeEmail')],
      ['Wrong email', i18n.t('WrongEmail')],
      ['Code already confirmed', i18n.t('CodeAlreadyConfirmed')],
      ['Wrong code', i18n.t('WrongCode')],
      ['default', i18n.t('ChangePasswordError')],
    ])
    const error = errorMap.has(message) ? errorMap.get(message) : errorMap.get('default')
    Alert.alert(
      i18n.t('RepairPassword'),
      error,
    )
  }

  const showNotification = (): void => {
    dispatch(toggleNotification({
      isVisible: true,
      message: i18n.t('WeSentCode'),
    }))
  }

  const repairPassword = async ({ email }: { email: string }): Promise<void> => {
    setIsProcessingRequest(true)
    const [error, result] = await dispatch<any>(sendCode({ email }))
    showNotification()
    setIsProcessingRequest(false)
    if (error) {
      return displayError(error)
    }
    if (result) {
      return setSentCode(result)
    }
  }

  const confirmCodeForChangePass = async ({ code, email }: { code?: number, email: string }) => {
    setIsProcessingRequest(true)
    const [error, token] = await dispatch<any>(confirmCode({ email, code: String(code) }))
    setIsProcessingRequest(false)
    if (error) {
      return displayError(error)
    }
    if (token) {
      navigation.navigate('AuthChangePassword', { token, email })
    }
  }

  const actionBlock = (handleSubmit: () => void) => {
    if (isProcessingRequest) {
      return (
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      )
    }

    return (
      <AppButton onPress={handleSubmit}>
        { i18n.t(isSentCode ? 'CreateNewPassword' : 'SendCode') }
      </AppButton>
    )
  }

  return (
    <View style={styles.root}>
      <Formik
        initialValues={{ email: '', code: '' }}
        onSubmit={isSentCode ? confirmCodeForChangePass : repairPassword}
        validationSchema={isSentCode ? validationSchemaWithCode : validationSchema}
      >
        {({
          handleChange, handleBlur, handleSubmit, values, errors, touched,
        }) => (
          <View style={styles.formWrapper}>
            <View style={styles.form}>
              <AppTextBold style={styles.title}>
                { i18n.t('RepairPassword') }
              </AppTextBold>
              <AppInput
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                error={touched.email && errors.email ? errors.email : ''}
                placeholder={i18n.t('EnterEmail')}
                textContentType="emailAddress"
              />
              {isSentCode && (
              <AppInput
                onChangeText={handleChange('code')}
                onBlur={handleBlur('code')}
                value={String(values.code)}
                error={touched.code && errors.code ? errors.code : ''}
                placeholder={i18n.t('EnterCode')}
              />
              )}
              { actionBlock(handleSubmit) }
            </View>
            {isSentCode && (
            <TouchableOpacity
              onPress={() => repairPassword({ email: values.email })}
            >
              <AppText>
                { i18n.t('ResendCode') }
              </AppText>
            </TouchableOpacity>
            )}
          </View>
        )}
      </Formik>
    </View>
  )
}
