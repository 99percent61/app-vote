import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  View,
  StyleSheet,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native'
import i18n from 'i18n-js'
import { AccountChooseAvatarProps } from './types'
import { RootState } from '../store'
import { User } from '../store/modules/user/types'
import { AppButton } from '../components/ui/AppButton'
import { AppText } from '../components/ui/AppText'
import { update, loadUser } from '../store/modules/user/actions'
import { ImageType } from '../store/modules/poll/types'
import logger from '../lib/logger'
import { uploadImages } from '../store/modules/files/actions'
import { THEME } from '../theme'
import { toggleNotification } from '../store/modules/ui/actions'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarWrapper: {
    borderRadius: 100,
    overflow: 'hidden',
    marginBottom: 20,
  },
  avatar: {
    width: 200,
    height: 200,
  },
  button: {
    width: '40%',
    marginBottom: 15,
  },
})

export const AccountChooseAvatar: React.FC<AccountChooseAvatarProps> = ({
  loadPermissions,
  pickImage,
  prepareImages,
  displayError,
}) => {
  const user = useSelector<RootState, User>((state) => state.user.current)
  const [avatar, setAvatar] = useState<ImageType>({ name: '', type: '', uri: user.avatar || '' })
  const [isVisible, setIsVisible] = useState<boolean>(false)
  const [isSavingAvatar, setSavingAvatar] = useState<boolean>(false)
  const dispatch = useDispatch()

  useEffect(() => {
    loadPermissions()
  }, [])

  const avatarBlock = () => {
    const { uri } = avatar
    if (uri) {
      return (
        <Image
          source={{ uri }}
          style={styles.avatar}
        />
      )
    }

    return (
      <AppText>
        { i18n.t('NoAvatar') }
      </AppText>
    )
  }

  const saveImage = (image: { name: string, type: string, uri: string }) => {
    setAvatar(image)
    setIsVisible(true)
  }

  const displaySuccess = (message: string): void => {
    dispatch(toggleNotification({
      isVisible: true,
      message,
    }))
  }

  const saveAvatarOnServer = async () => {
    if (avatar.type && avatar.uri) {
      setSavingAvatar(true)
      const preparedAvatar = prepareImages([avatar])
      const [imageError, links] = await dispatch<any>(uploadImages(preparedAvatar))
      if (imageError) {
        let message = i18n.t('UploadImageError')
        if (imageError.includes('forbidden image type')) {
          message = i18n.t('ForbiddenImageType')
        }
        setSavingAvatar(false)
        return displayError(message)
      }

      const [error] = await dispatch<any>(update({ avatar: links[0] }))
      setSavingAvatar(false)
      if (error) {
        return displayError(i18n.t('SaveAvatarError'))
      }
      displaySuccess(i18n.t('SuccefullySaved'))
      setIsVisible(false)
      dispatch<any>(loadUser())
    } else {
      logger.error('Incomplete avatar object ', JSON.stringify(avatar))
      displayError(i18n.t('SaveAvatarError'))
    }
  }

  const saveBlock = () => {
    if (isSavingAvatar) {
      return (
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      )
    }
    if (isVisible) {
      return (
        <AppButton
          style={styles.button}
          onPress={saveAvatarOnServer}
        >
          { i18n.t('Save') }
        </AppButton>
      )
    }
    return null
  }

  return (
    <View style={styles.root}>
      <View style={styles.avatarWrapper}>
        { avatarBlock() }
      </View>
      <AppButton
        style={styles.button}
        onPress={() => pickImage(saveImage)}
      >
        { i18n.t('ChooseAvatar') }
      </AppButton>
      { saveBlock() }
    </View>
  )
}
