import React, { useEffect, useState } from 'react'
import {
  View, StyleSheet, Dimensions, ActivityIndicator, ScrollView,
} from 'react-native'
import { useDispatch } from 'react-redux'
import i18n from 'i18n-js'
import { PieChart } from 'react-native-chart-kit'
import { AppText } from '../components/ui/AppText'
import { AnswerStatisticsProps } from './types'
import { getStatisticsByPollId } from '../store/modules/poll/actions'
import { toggleNotification } from '../store/modules/ui/actions'
import { THEME } from '../theme'
import { Answer, StatisticsType } from '../store/modules/poll/types'
import { AppTextBold } from '../components/ui/AppTextBold'

const screenWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  chart: {
    marginBottom: 15,
  },
  answerCharts: {
    padding: 10,
    margin: 10,
    marginVertical: 20,
    borderRadius: 20,
    backgroundColor: THEME.colors.WHITE,
    shadowColor: THEME.colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 13,
  },
  answerTitle: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 5,
  },
  statisticsType: {
    textAlign: 'center',
  },
})

export const AnswerStatistics: React.FC<AnswerStatisticsProps> = ({ route }) => {
  const [statistics, setStatistics] = useState<Record<string, Record<string, any>> | null>(null)
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const dispatch = useDispatch()

  const fetchStatistics = async (id: number) => {
    setIsLoading(true)
    const [error, result] = await dispatch<any>(getStatisticsByPollId({ pollId: id }))
    setIsLoading(false)
    if (error) {
      return toggleNotification({ isVisible: true, message: i18n.t('ErrorFetchStatistics') })
    }

    return setStatistics(result)
  }

  useEffect(() => {
    const { poll } = route.params
    fetchStatistics(poll.id)
  }, [route])

  if (isLoading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      </View>
    )
  }

  if (statistics === null) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <AppText>
          { i18n.t('HaveNotVotes') }
        </AppText>
      </View>
    )
  }

  const getGenderColor = (gender: string): string => {
    const dictionary: Record<string, string> = {
      male: 'purple',
      female: 'green',
      default: '#1ddeb7',
    }

    return dictionary[gender] || dictionary.default
  }

  const getAgeColor = (ageKey: string): string => {
    const dictionary: Record<string, string> = {
      '<20': 'pink',
      '20-30': 'orange',
      '30-40': 'red',
      '40-50': 'brown',
      '50-60': 'yellow',
      '>60': 'black',
      default: 'blue',
    }

    return dictionary[ageKey] || dictionary.default
  }

  const getStatisticsByAnswerId = (id: number) => statistics[id]
  const toArrayByType = (
    data: Record<string, Record<string, number>>,
    type: StatisticsType,
  ): Record<string, any>[] => {
    const result: Record<string, any>[] = []
    if (data[type] === undefined) return result

    Object.keys(data[type]).forEach((key) => {
      result.push({
        name: type === StatisticsType.GENDER ? i18n.t(key) : key,
        votes: data[type][key],
        color: type === StatisticsType.AGE ? getAgeColor(key) : getGenderColor(key),
        legendFontColor: '#7F7F7F',
        legendFontSize: 15,
      })
    })

    return result
  }

  const chartConfig = {
    backgroundGradientFrom: 'white',
    backgroundGradientTo: 'black',
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  }

  const labelForStatistcsType = {
    age: 'Age',
    gender: 'Gender',
  }

  const renderPieChart = (
    {
      answerId,
      statisticsType,
      data,
    }: {
      answerId: number,
      statisticsType: StatisticsType,
      data: Record<string, any>[],
    },
  ) => (
    <View key={answerId + statisticsType}>
      <AppText style={styles.statisticsType}>
        { i18n.t(labelForStatistcsType[statisticsType]) }
      </AppText>
      <PieChart
        data={data}
        width={screenWidth - 40}
        height={200}
        chartConfig={chartConfig}
        accessor="votes"
        backgroundColor="transparent"
        paddingLeft="0"
        style={styles.chart}
      />
    </View>
  )

  const renderCharts = (answers: Answer[]) => answers.map((answer) => {
    const statisticsByAnswerId = getStatisticsByAnswerId(answer.id)
    if (statisticsByAnswerId === undefined) return null

    const result: Record<string, any>[] = []
    Object.keys(StatisticsType).forEach((key) => {
      const lowerCaseKey = key.toLowerCase()
      if (statisticsByAnswerId[lowerCaseKey]) {
        const data = toArrayByType(statisticsByAnswerId, lowerCaseKey as StatisticsType)
        result.push(
          renderPieChart(
            {
              answerId: answer.id,
              statisticsType: lowerCaseKey as StatisticsType,
              data,
            },
          ),
        )
      }
    })

    return (
      <View
        key={answer.id}
        style={styles.answerCharts}
      >
        <AppTextBold style={styles.answerTitle}>
          { answer.title }
        </AppTextBold>
        { result }
      </View>
    )
  })

  return (
    <ScrollView>
      { renderCharts(route.params.poll.answers) }
    </ScrollView>
  )
}
