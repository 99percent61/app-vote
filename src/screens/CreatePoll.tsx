import React, { useState } from 'react'
import {
  View,
  ScrollView,
  StyleSheet,
  Alert,
  ActivityIndicator,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { Formik, FormikHelpers } from 'formik'
import * as Yup from 'yup'
import i18n from 'i18n-js'
import uniqueId from 'lodash.uniqueid'
import pick from 'lodash.pick'
import { CreatePollProps } from './types'
import { AppInput } from '../components/ui/AppInput'
import { AppButton } from '../components/ui/AppButton'
import { Answer, Poll, ImageType } from '../store/modules/poll/types'
import { createPoll } from '../store/modules/poll/actions'
import { uploadImages } from '../store/modules/files/actions'
import logger from '../lib/logger'
import { PhotoPicker } from '../components/photoPicker/PhotoPicker'
import { AppIcon } from '../components/ui/AppIcon'
import { THEME } from '../theme'
import { AppTextBold } from '../components/ui/AppTextBold'
import { AppText } from '../components/ui/AppText'
import { RootState } from '../store'
import { User } from '../store/modules/user/types'
import { isIos } from '../lib/platform'
import PrivacyTermsModal from '../components/user/privacyTermsModal'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 20,
    paddingBottom: 50,
  },
  blockWrapper: {
    marginVertical: 10,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#999999',
  },
  title: {
    marginBottom: 10,
  },
  inputWrapper: {
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#999999',
  },
  input: {
    fontFamily: 'nunito-regular',
    width: 300,
    fontSize: 18,
    fontWeight: '700',
  },
  answersWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  answer: {
    flexDirection: 'row',
  },
  addAnswerWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  addIcon: {
    marginRight: 10,
  },
  createButtonWrapper: {
    alignItems: 'center',
    marginTop: 10,
    paddingHorizontal: 10,
  },
  createButton: {
    width: 250,
    maxWidth: '100%',
    height: 45,
  },
})

export const CreatePoll: React.FC<CreatePollProps> = ({ navigation }) => {
  const dispatch = useDispatch()
  const user = useSelector<RootState, User>((state) => state.user.current)
  const [isVisibleModal, setVisibilityModal] = useState<boolean>(false)
  const getUniqueId = (): number => (Number(uniqueId()))
  const [answers, setAnswers] = useState<Answer[]>([{
    id: getUniqueId(),
    title: '',
    error: '',
    votes: 0,
  }])
  const [images, setImages] = useState<ImageType[]>([])
  const [isCreatingPoll, setCreatingPoll] = useState<boolean>(false)

  const validationSchemaTitle = Yup.object({
    title: Yup.string()
      .required(i18n.t('RequiredString')),
  })

  const validationSchemaAnswer = Yup.object({
    id: Yup.number().required(i18n.t('RequiredString')),
    title: Yup.string().required(i18n.t('RequiredString')),
  })

  const validationSchemaAnswers = Yup.array()
    .of(validationSchemaAnswer)
    .required(i18n.t('RequiredString'))
    .min(1, i18n.t('MinCountAnswers', { count: 1 }))
    .max(10, i18n.t('MaxCountAnswers', { count: 10 }))

  const displayError = (message: string): void => {
    Alert.alert(
      i18n.t('CreatePoll'),
      message,
    )
  }

  const prepareImages = (imageList: ImageType[]): FormData => {
    const preparedImages = new FormData()
    imageList.forEach((image) => {
      preparedImages.append('photo', image)
    })
    return preparedImages
  }

  const resetAnswers = (): void => {
    setAnswers([{
      id: getUniqueId(),
      title: '',
      error: '',
      votes: 0,
    }])
  }

  const create = async (
    values: { title: string },
    actions: FormikHelpers<{ title: string }>,
  ) => {
    const isValidAnswers = await validationSchemaAnswers.isValid(answers)
    if (!isValidAnswers) {
      return displayError(i18n.t('CheckCorrectnessOfAnswers'))
    }

    if (!user.terms) {
      return setVisibilityModal(true)
    }

    setCreatingPoll(true)
    let imageLinks = []
    if (images && images.length) {
      const preparedImages = prepareImages(images)
      const [imageError, links] = await dispatch<any>(uploadImages(preparedImages))
      if (imageError) {
        let message = i18n.t('UploadImageError')
        if (imageError.includes('forbidden image type')) {
          message = i18n.t('ForbiddenImageType')
        }
        setCreatingPoll(false)
        return displayError(message)
      }
      imageLinks = links
    }

    const poll: Poll = {
      title: values.title,
      answers: answers.map((item) => pick(item, ['id', 'title', 'votes'])),
      time_limit: 0,
      image: imageLinks,
    }
    const [error] = await dispatch<any>(createPoll(poll))
    setCreatingPoll(false)
    if (error) {
      return displayError(i18n.t('CreatePollError'))
    }
    navigation.navigate('World', {
      screen: 'AllPolls',
      params: {
        updatePollList: true,
      },
    })
    actions.resetForm()
    resetAnswers()
    setImages([])
  }

  const deleteAnswerById = (id: number): void => {
    setAnswers(answers.filter((answer) => answer.id !== id))
  }

  const setTitleById = (title: string, id: number): void => {
    const copy = answers.map((answer) => {
      let error = ''
      try {
        validationSchemaAnswer.validateSync({ id, title }, { abortEarly: false })
      } catch (err) {
        if (err.name === 'ValidationError') {
          error = err.inner[0].message
        } else {
          logger.error(err)
        }
      }
      if (answer.id === id) {
        return { ...answer, title, error }
      }
      return answer
    })
    setAnswers(copy)
  }

  const addEmptyAnswer = (): void => {
    setAnswers([...answers, {
      id: getUniqueId(),
      title: '',
      error: '',
      votes: 0,
    }])
  }

  const photoPickHandler = (image: ImageType): void => {
    setImages([...images, image])
  }

  const removeImage = (uri: string): void => {
    setImages(images.filter((image) => image.uri !== uri))
  }

  const selectedImages = (): string[] => (images.map((image) => image.uri))

  const actionBlock = (handleSubmit: () => void, btnTitle: string = 'Create') => (
    <View style={styles.createButtonWrapper}>
      {
        isCreatingPoll
          ? (<ActivityIndicator size="large" color={THEME.colors.BLUE} />)
          : (
            <AppButton
              style={styles.createButton}
              onPress={handleSubmit}
            >
              { i18n.t(btnTitle) }
            </AppButton>
          )
      }
    </View>
  )

  return (
    <KeyboardAvoidingView
      behavior={isIos ? 'padding' : 'height'}
    >
      <ScrollView>
        <View style={styles.root}>
          <Formik
            initialValues={{ title: '' }}
            onSubmit={create}
            validationSchema={validationSchemaTitle}
          >
            {({
              handleChange, handleBlur, handleSubmit, values, errors, touched,
            }) => (
              <View>
                <View style={styles.inputWrapper}>
                  <TextInput
                    onChangeText={handleChange('title')}
                    onBlur={handleBlur('title')}
                    value={values.title}
                    placeholder={i18n.t('WhatDoYouThink')}
                    maxLength={120}
                    multiline
                    numberOfLines={2}
                    textAlign="center"
                    style={styles.input}
                  />
                  <AppTextBold style={THEME.text.error}>
                    { touched.title && errors.title ? errors.title : '' }
                  </AppTextBold>
                </View>
                <View style={styles.blockWrapper}>
                  <AppTextBold style={styles.title}>
                    { i18n.t('Media') }
                  </AppTextBold>
                  <PhotoPicker
                    onPick={photoPickHandler}
                    onRemove={removeImage}
                    selectedImages={selectedImages()}
                  />
                </View>
                <View style={styles.blockWrapper}>
                  <AppTextBold style={styles.title}>
                    { i18n.t('Answers') }
                  </AppTextBold>
                  <View style={styles.answersWrapper}>
                    {
                      answers.map((answer) => (
                        <View
                          key={answer.id}
                          style={styles.answer}
                        >
                          <AppInput
                            onChangeText={(text) => setTitleById(text, answer.id)}
                            onBlur={(event) => setTitleById(event.nativeEvent.text, answer.id)}
                            value={answer.title}
                            placeholder={i18n.t('Awsome')}
                            error={answer.error}
                            icon="remove-circle-outline"
                            iconColor="red"
                            onPressIcon={() => deleteAnswerById(answer.id)}
                          />
                        </View>
                      ))
                    }
                  </View>
                  <TouchableOpacity
                    style={styles.addAnswerWrapper}
                    onPress={addEmptyAnswer}
                  >
                    <AppIcon
                      name="add-circle-outline"
                      color={THEME.MAIN_COLOR}
                      size={30}
                      onPress={addEmptyAnswer}
                      style={styles.addIcon}
                    />
                    <AppText style={{ color: THEME.MAIN_COLOR }}>
                      { i18n.t('AddAnswer') }
                    </AppText>
                  </TouchableOpacity>
                </View>
                { actionBlock(handleSubmit) }
                <PrivacyTermsModal
                  isVisible={isVisibleModal}
                  setVisibility={setVisibilityModal}
                  afterAccept={handleSubmit}
                />
              </View>
            )}
          </Formik>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}
