import React, { useState, useLayoutEffect } from 'react'
import {
  View,
  StyleSheet,
  TextInput,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native'
import { useDispatch } from 'react-redux'
import i18n from 'i18n-js'
import { BaseProps } from './types'
import { AppTextBold } from '../components/ui/AppTextBold'
import { THEME } from '../theme'
import { changePasswordByPass } from '../store/modules/user/actions'
import { AppText } from '../components/ui/AppText'
import { isIos, isAndroid } from '../lib/platform'
import { AppButton } from '../components/ui/AppButton'
import { AppIcon } from '../components/ui/AppIcon'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  itemWrapper: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: THEME.colors.WHITE,
  },
  title: {
    flex: 1,
  },
  input: {
    flex: 1,
  },
  doneIos: {
    marginRight: 10,
    color: isIos ? THEME.colors.BLACK : THEME.colors.WHITE,
  },
  doneAndroid: {
    alignItems: 'center',
    marginTop: 15,
  },
  icon: {
    flex: 1,
  },
})

export const AccountChangePassword: React.FC<BaseProps> = ({ navigation }) => {
  const [currentPassword, setCurrentPassword] = useState<string>('')
  const [newPassword, setNewPassword] = useState<string>('')
  const [repeatedPassword, setRepeatedPassword] = useState<string>('')
  const [isChangingPass, setChangingPass] = useState<boolean>(false)
  const [securityText, setSecurityText] = useState<string[]>(['current', 'new', 'confirm'])
  const dispatch = useDispatch()

  const displayMessage = (message: string) => {
    Alert.alert(
      i18n.t('Password'),
      message,
    )
  }

  const validate = () => {
    if (
      !currentPassword
      || !newPassword
      || !repeatedPassword
      || (newPassword !== repeatedPassword)
    ) {
      displayMessage(i18n.t('CheckEnteredData'))
      return false
    }

    return true
  }

  const handleError = (error: string): void => {
    const message = error.toLowerCase()
    if (message.includes('wrong email')) {
      displayMessage(i18n.t('UserDoesNotExist'))
    } else if (message.includes('wrong current')) {
      displayMessage(i18n.t('WrongCurrentPassword'))
    } else if (message.includes('new password must be')) {
      displayMessage(i18n.t('NewPassMustBeDifferent'))
    } else {
      displayMessage(i18n.t('ChangePassError'))
    }
  }

  const changePassword = async () => {
    const isValid = validate()
    if (isValid) {
      setChangingPass(true)
      const [err] = await dispatch<any>(changePasswordByPass({
        currentPassword,
        newPassword,
        repeatedPassword,
      }))
      setChangingPass(false)
      if (err) {
        return handleError(err)
      }

      displayMessage(i18n.t('PasswordChanged'))
      navigation.goBack()
    }
  }

  useLayoutEffect(() => {
    if (isIos) {
      navigation.setOptions({
        headerRight: () => (
          <TouchableOpacity
            onPress={() => changePassword()}
          >
            <AppText style={styles.doneIos}>
              { i18n.t('Done') }
            </AppText>
          </TouchableOpacity>
        ),
      })
    }
  }, [navigation, isIos, currentPassword, newPassword, repeatedPassword])

  const actionBlock = () => {
    if (isAndroid) {
      if (isChangingPass) {
        return (
          <View style={styles.doneAndroid}>
            <ActivityIndicator size="large" color={THEME.colors.BLUE} />
          </View>
        )
      }

      return (
        <View style={styles.doneAndroid}>
          <AppButton
            style={{ width: '40%' }}
            onPress={changePassword}
          >
            { i18n.t('Done') }
          </AppButton>
        </View>
      )
    }

    return null
  }

  const toggleSecurityText = (id: 'current' | 'new' | 'confirm'): void => {
    if (securityText.includes(id)) {
      setSecurityText(securityText.filter((item) => item !== id))
    } else {
      setSecurityText([...securityText, id])
    }
  }

  const isVisiblePassword = (id: 'current' | 'new' | 'confirm'): boolean => (
    securityText.includes(id)
  )

  return (
    <KeyboardAvoidingView
      behavior={ isIos ? 'padding' : 'height' }
      style={styles.root}
    >
      <View>
        <View style={styles.itemWrapper}>
          <AppTextBold style={styles.title}>
            { i18n.t('CurrentPassword') }
          </AppTextBold>
          <TextInput
            autoFocus
            style={styles.input}
            onChangeText={setCurrentPassword}
            value={currentPassword}
            placeholder={i18n.t('CurrentPassword')}
            textContentType="password"
            secureTextEntry={isVisiblePassword('current')}
          />
          <AppIcon
            name="visibility"
            size={22}
            color={THEME.colors.BLACK}
            onPress={() => toggleSecurityText('current')}
            style={styles.icon}
          />
        </View>
        <View style={styles.itemWrapper}>
          <AppTextBold style={styles.title}>
            { i18n.t('NewPassword') }
          </AppTextBold>
          <TextInput
            style={styles.input}
            onChangeText={setNewPassword}
            value={newPassword}
            placeholder={i18n.t('NewPassword')}
            textContentType="newPassword"
            secureTextEntry={isVisiblePassword('new')}
          />
          <AppIcon
            name="visibility"
            size={22}
            color={THEME.colors.BLACK}
            onPress={() => toggleSecurityText('new')}
            style={styles.icon}
          />
        </View>
        <View style={styles.itemWrapper}>
          <AppTextBold style={styles.title}>
            { i18n.t('ConfirmPassword') }
          </AppTextBold>
          <TextInput
            style={styles.input}
            onChangeText={setRepeatedPassword}
            value={repeatedPassword}
            placeholder={i18n.t('ConfirmPassword')}
            textContentType="newPassword"
            secureTextEntry={isVisiblePassword('confirm')}
          />
          <AppIcon
            name="visibility"
            size={22}
            color={THEME.colors.BLACK}
            onPress={() => toggleSecurityText('confirm')}
            style={styles.icon}
          />
        </View>
        { actionBlock() }
      </View>
    </KeyboardAvoidingView>
  )
}
