import React from 'react'
import { View, StyleSheet } from 'react-native'
import { SubscribePollsProps } from './types'
import { AppText } from '../components/ui/AppText'
import { AppActionButton } from '../components/ui/AppActionButton'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export const SubscribePolls: React.FC<SubscribePollsProps> = ({ navigation }) => (
  <View style={styles.root}>
    <AppText>
      Subscribe Polls
    </AppText>
    <AppActionButton
      onPress={() => navigation.navigate('CreatePoll')}
    />
  </View>
)
