import React, { useState } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native'
import * as Linking from 'expo-linking'
import { useDispatch } from 'react-redux'
import { Formik, FormikHelpers } from 'formik'
import * as Yup from 'yup'
import i18n from 'i18n-js'
import { BaseProps } from './types'
import { AppInput } from '../components/ui/AppInput'
import { AppButton } from '../components/ui/AppButton'
import { feedback } from '../store/modules/user/actions'
import { THEME } from '../theme';
import logger from '../lib/logger';
import { toggleNotification } from '../store/modules/ui/actions';
import { isIos } from '../lib/platform'
import { AppText } from '../components/ui/AppText'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  wrapper: {
    alignItems: 'center',
  },
  messageInput: {
    maxHeight: 100,
  },
  button: {
    marginTop: 10,
    width: 250,
    maxWidth: '100%',
    height: 45,
  },
  termsWrapper: {
    marginTop: 20,
  },
  link: {
    color: THEME.colors.BLUE,
    paddingRight: 5,
    paddingLeft: 5,
  },
  terms: {
    display: 'flex',
    flexDirection: 'row',
  },
})

export const Feedback: React.FC<BaseProps> = () => {
  const [isInProcess, setIsInProcess] = useState<boolean>(false)
  const dispatch = useDispatch()
  const validationSchema = Yup.object({
    title: Yup.string()
      .required(i18n.t('RequiredString')),
    email: Yup.string(),
    message: Yup.string()
      .required(i18n.t('RequiredString')),
  })

  const openPrivacyPolicy = () => {
    Linking.openURL('https://affect.flycricket.io/privacy.html')
  }

  const openTerms = () => {
    Linking.openURL('https://affect.flycricket.io/terms.html')
  }

  const sendFeedback = async (
    values: { title: string, message: string, email: string },
    actions: FormikHelpers<{ title: string, message: string, email: string }>,
  ): Promise<void> => {
    setIsInProcess(true)
    const [error] = await dispatch<any>(feedback(values))
    setIsInProcess(false)
    if (error) {
      logger.error(error, 'feedback')
      dispatch(toggleNotification({
        isVisible: true,
        message: i18n.t('ErrorSendFeedback'),
      }))
      return
    }

    actions.resetForm()
    dispatch(toggleNotification({
      isVisible: true,
      message: i18n.t('FeedbackSuccessSent'),
    }))
  }

  const actionBlock = (
    handleSubmit: (e?: React.FormEvent<HTMLFormElement> | undefined) => void,
  ) => {
    if (isInProcess) {
      return (
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      )
    }

    return (
      <AppButton
        style={styles.button}
        onPress={handleSubmit}
      >
        { i18n.t('Send') }
      </AppButton>
    )
  }

  return (
    <KeyboardAvoidingView
      behavior={ isIos ? 'padding' : 'height' }
      style={styles.root}
    >
      <ScrollView
        contentContainerStyle={styles.root}
      >
        <Formik
          initialValues={{ title: '', email: '', message: '' }}
          onSubmit={sendFeedback}
          validationSchema={validationSchema}
        >
          {({
            handleChange, handleBlur, handleSubmit, values, errors, touched,
          }) => (
            <View style={styles.wrapper}>
              <AppInput
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                error={touched.email && errors.email ? errors.email : ''}
                title={i18n.t('Email')}
                keyboardType="email-address"
              />
              <AppInput
                value={values.title}
                onChangeText={handleChange('title')}
                onBlur={handleBlur('title')}
                error={touched.title && errors.title ? errors.title : ''}
                title={i18n.t('Title')}
              />
              <AppInput
                value={values.message}
                onChangeText={handleChange('message')}
                onBlur={handleBlur('message')}
                error={touched.message && errors.message ? errors.message : ''}
                title={i18n.t('Message')}
                multiline
                numberOfLines={4}
                style={styles.messageInput}
              />
              { actionBlock(handleSubmit) }
            </View>
          )}
        </Formik>
        <View style={styles.termsWrapper}>
          <AppText>
            { i18n.t('TermsReadMore') }
          </AppText>
          <View style={styles.terms}>
            <TouchableOpacity
              onPress={openPrivacyPolicy}
            >
              <AppText style={styles.link}>
                { i18n.t('PrivacyPolicy') }
              </AppText>
            </TouchableOpacity>
            <AppText>and</AppText>
            <TouchableOpacity
              onPress={openTerms}
            >
              <AppText style={styles.link}>
                { i18n.t('Terms') }
              </AppText>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}
