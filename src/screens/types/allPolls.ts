import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  AllPols: {
    updatePollList: boolean
  }
}

type AllPollsScreenRouteProp = RouteProp<RootStackParamList, 'AllPols'>
type AllPollsScreenNavigationProp = StackNavigationProp<RootStackParamList, 'AllPols'>

export type AllPollsProps = {
  route: AllPollsScreenRouteProp
  navigation: AllPollsScreenNavigationProp
}
