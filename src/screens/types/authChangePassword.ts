import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  AuthChangePassword: {
    token: string,
    email: string
  }
}

type AuthChangePasswordScreenRouteProp = RouteProp<RootStackParamList, 'AuthChangePassword'>
type AuthChangePasswordScreenNavigationProp = StackNavigationProp<RootStackParamList, 'AuthChangePassword'>

export type AuthChangePasswordProps = {
  route: AuthChangePasswordScreenRouteProp
  navigation?: AuthChangePasswordScreenNavigationProp
}
