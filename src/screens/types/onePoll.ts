import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  OnePoll: {
    pollId: number
  }
}

type OnePollScreenRouteProp = RouteProp<RootStackParamList, 'OnePoll'>
type OnePollScreenNavigationProp = StackNavigationProp<RootStackParamList, 'OnePoll'>

export type OnePollProps = {
  route: OnePollScreenRouteProp
  navigation: OnePollScreenNavigationProp
}
