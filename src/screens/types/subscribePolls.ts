import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  SubscribePolls: undefined
}

type SubscribePollsScreenRouteProp = RouteProp<RootStackParamList, 'SubscribePolls'>
type SubscribePollsScreenNavigationProp = StackNavigationProp<RootStackParamList, 'SubscribePolls'>

export type SubscribePollsProps = {
  route?: SubscribePollsScreenRouteProp
  navigation?: SubscribePollsScreenNavigationProp
}
