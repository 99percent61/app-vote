import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  Auth: undefined
}

type AuthScreenRouteProp = RouteProp<RootStackParamList, 'Auth'>
type AuthScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Auth'>

export type AuthProps = {
  route?: AuthScreenRouteProp
  navigation?: AuthScreenNavigationProp
}
