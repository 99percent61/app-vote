import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'
import { Poll } from '../../store/modules/poll/types'

type RootStackParamList = {
  AnswerStatistics: {
    poll: Poll
  }
}

type AnswerStatisticsScreenRouteProp = RouteProp<RootStackParamList, 'AnswerStatistics'>
type AnswerStatisticsScreenNavigationProp = StackNavigationProp<RootStackParamList, 'AnswerStatistics'>

export type AnswerStatisticsProps = {
  route: AnswerStatisticsScreenRouteProp
  navigation: AnswerStatisticsScreenNavigationProp
}
