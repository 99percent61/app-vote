import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  Registration: undefined
}

type RegistrationScreenRouteProp = RouteProp<RootStackParamList, 'Registration'>
type RegistrationScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Registration'>

export type RegistrationProps = {
  route?: RegistrationScreenRouteProp
  navigation?: RegistrationScreenNavigationProp
}
