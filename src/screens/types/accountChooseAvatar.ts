import { BaseProps } from './baseProps'
import { ImageType } from '../../store/modules/poll/types'

export interface AccountChooseAvatarProps extends BaseProps {
  pickImage: (
    callback: (props: { name: string, type: string, uri: string }) => void
  ) => Promise<void>
  loadPermissions: () => Promise<void>
  displayError: (message: string) => void
  prepareImages: (images: ImageType[]) => FormData
}
