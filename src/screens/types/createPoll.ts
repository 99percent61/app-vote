import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  CreatePoll: undefined
}

type CreatePollScreenRouteProp = RouteProp<RootStackParamList, 'CreatePoll'>
type CreatePollScreenNavigationProp = StackNavigationProp<RootStackParamList, 'CreatePoll'>

export type CreatePollProps = {
  route?: CreatePollScreenRouteProp
  navigation?: CreatePollScreenNavigationProp
}
