import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  Account: {
    updatePollList: boolean
  }
}

export type AccountScreenRouteProp = RouteProp<RootStackParamList, 'Account'>
type AccountScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Account'>

export type AccountProps = {
  route: AccountScreenRouteProp
  navigation: AccountScreenNavigationProp
}
