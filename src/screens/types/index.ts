import { AuthProps } from './auth'
import { LoginProps } from './login'
import { RegistrationProps } from './registration'
import { AccountProps } from './account'
import { AllPollsProps } from './allPolls'
import { SubscribePollsProps } from './subscribePolls'
import { AuthForgotPasswordProps } from './authForgotPassword'
import { AuthChangePasswordProps } from './authChangePassword'
import { CreatePollProps } from './createPoll'
import { OnePollProps } from './onePoll'
import { BaseProps } from './baseProps'
import { AccountChooseAvatarProps } from './accountChooseAvatar'
import { AnswerStatisticsProps } from './answerStatistics'

export {
  AuthProps,
  LoginProps,
  RegistrationProps,
  AccountProps,
  AllPollsProps,
  SubscribePollsProps,
  AuthForgotPasswordProps,
  AuthChangePasswordProps,
  CreatePollProps,
  OnePollProps,
  BaseProps,
  AccountChooseAvatarProps,
  AnswerStatisticsProps,
}
