import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  AuthForgotPassword: undefined
}

type AuthForgotPasswordScreenRouteProp = RouteProp<RootStackParamList, 'AuthForgotPassword'>
type AuthForgotPasswordScreenNavigationProp = StackNavigationProp<RootStackParamList, 'AuthForgotPassword'>

export type AuthForgotPasswordProps = {
  route?: AuthForgotPasswordScreenRouteProp
  navigation?: AuthForgotPasswordScreenNavigationProp
}
