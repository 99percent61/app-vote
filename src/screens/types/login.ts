import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  Login: undefined
}

type LoginScreenRouteProp = RouteProp<RootStackParamList, 'Login'>
type LoginScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Login'>

export type LoginProps = {
  route?: LoginScreenRouteProp
  navigation?: LoginScreenNavigationProp
}
