import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack/src/types'

type RootStackParamList = {
  Base: undefined
}

type BaseScreenRouteProp = RouteProp<RootStackParamList, 'Base'>
type BaseScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Base'>

export interface BaseProps {
  route: BaseScreenRouteProp
  navigation: BaseScreenNavigationProp
}
