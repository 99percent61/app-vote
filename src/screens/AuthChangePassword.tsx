import React, { useState } from 'react'
import {
  StyleSheet,
  View,
  Alert,
  ActivityIndicator,
} from 'react-native'
import { useDispatch } from 'react-redux'
import { Formik } from 'formik'
import * as Yup from 'yup'
import i18n from 'i18n-js'
import { AuthChangePasswordProps } from './types'
import { AppInput } from '../components/ui/AppInput'
import { AppButton } from '../components/ui/AppButton'
import { changePasswordByToken } from '../store/modules/user/actions'
import { AppTextBold } from '../components/ui/AppTextBold'
import { THEME } from '../theme'
import { toggleNotification } from '../store/modules/ui/actions'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 18,
    marginBottom: 20,
  },
})

export const AuthChangePassword: React.FC<AuthChangePasswordProps> = ({ route, navigation }) => {
  const [isProcessingRequest, setIsProcessingRequest] = useState(false)
  const validationSchema = Yup.object({
    password: Yup.string()
      .min(8, i18n.t('AtLeast8'))
      .max(50, i18n.t('TooLong'))
      .required(i18n.t('RequiredString')),
    repeatedPassword: Yup
      .string()
      .required(i18n.t('RequiredString'))
      .oneOf([Yup.ref('password')], i18n.t('PasswordsMatch')),
  })

  const { token, email } = route.params
  const dispatch = useDispatch()

  const displayError = (message: string): void => {
    const errorMap = new Map([
      ['email must be a valid email', i18n.t('NotLikeEmail')],
      ['Wrong email', i18n.t('WrongEmail')],
      ['Code did not confirm', i18n.t('CodeNotConfirm')],
      ['Token does not active', i18n.t('TokenNotActive')],
      ['Wrong token', i18n.t('WrongToken')],
      ['default', i18n.t('ChangePasswordError')],
    ])
    const error = errorMap.has(message) ? errorMap.get(message) : errorMap.get('default')
    Alert.alert(
      i18n.t('RepairPassword'),
      error,
    )
  }

  const showNotification = (): void => {
    dispatch(toggleNotification({
      isVisible: true,
      message: i18n.t('PasswordChanged'),
    }))
  }

  const changePassword = async (
    { password, repeatedPassword }: { password: string, repeatedPassword: string },
  ) => {
    setIsProcessingRequest(true)
    const [error, result] = await dispatch<any>(changePasswordByToken({
      email,
      password,
      token,
      repeatedPassword,
    }))
    setIsProcessingRequest(false)
    if (error) {
      return displayError(error)
    }
    if (result) {
      showNotification()
      navigation.popToTop()
    }
  }

  const actionBlock = (handleSubmit: () => void) => {
    if (isProcessingRequest) {
      return (
        <ActivityIndicator size="large" color={THEME.colors.BLUE} />
      )
    }

    return (
      <AppButton onPress={handleSubmit}>
        { i18n.t('ChangePassword') }
      </AppButton>
    )
  }

  return (
    <Formik
      initialValues={{ password: '', repeatedPassword: '' }}
      onSubmit={changePassword}
      validationSchema={validationSchema}
    >
      {({
        handleChange, handleBlur, handleSubmit, values, errors, touched,
      }) => (
        <View style={styles.root}>
          <AppTextBold style={styles.title}>
            { i18n.t('CreateNewPass') }
          </AppTextBold>
          <AppInput
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            value={values.password}
            error={touched.password && errors.password ? errors.password : ''}
            placeholder={i18n.t('EnterPassword')}
            textContentType="newPassword"
            secureTextEntry
            withIconEye
          />
          <AppInput
            onChangeText={handleChange('repeatedPassword')}
            onBlur={handleBlur('repeatedPassword')}
            value={values.repeatedPassword}
            error={touched.repeatedPassword && errors.repeatedPassword ? errors.repeatedPassword : ''}
            placeholder={i18n.t('RepeatPassword')}
            textContentType="newPassword"
            secureTextEntry
            withIconEye
          />
          { actionBlock(handleSubmit) }
        </View>
      )}
    </Formik>
  )
}
