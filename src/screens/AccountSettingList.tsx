import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native'
import i18n from 'i18n-js'
import { logout } from '../store/modules/user/actions'
import { RootState } from '../store'
import { User } from '../store/modules/user/types'
import { AccountProps } from './types'
import { AppListItem } from '../components/ui/AppListItem'
import { THEME } from '../theme'
import { AppTextBold } from '../components/ui/AppTextBold'

const styles = StyleSheet.create({
  root: {
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  author: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  avatarWrapper: {
    borderRadius: 50,
    overflow: 'hidden',
    marginBottom: 10,
  },
  avatar: {
    width: 80,
    height: 80,
  },
  textWrapper: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: THEME.colors.WHITE,
  },
  text: {
    fontSize: 18,
  },
  logout: {
    color: THEME.DANGER_COLOR,
  },
})

export const AccountSettingList: React.FC<AccountProps> = ({ navigation }) => {
  const user = useSelector<RootState, User>((state) => state.user.current)
  const dispatch = useDispatch()
  const userLogout = () => {
    dispatch(logout())
  }

  const getUserAvatar = () => {
    let avatar = require('../../assets/person.png')
    if (user && user.avatar) {
      avatar = {
        uri: user.avatar,
      }
    }
    return avatar
  }

  const renderSettings = () => {
    const settingList = [
      { title: i18n.t('Profile'), action: () => navigation.navigate('AccountProfile'), style: {} },
      { title: i18n.t('Support'), action: () => navigation.navigate('AccountFeedback'), style: {} },
      { title: i18n.t('Logout'), action: userLogout, style: styles.logout },
    ]

    return settingList.map((el) => (
      <TouchableOpacity
        onPress={el.action}
        key={el.title}
        style={styles.textWrapper}
      >
        <AppListItem
          style={{ ...styles.text, ...el.style }}
          onPress={el.action}
        >
          { el.title }
        </AppListItem>
      </TouchableOpacity>
    ))
  }

  return (
    <ScrollView style={styles.root}>
      <View style={styles.author}>
        <TouchableOpacity
          style={styles.avatarWrapper}
          onPress={() => navigation.navigate('AccountChooseAvatar')}
        >
          <Image
            source={getUserAvatar()}
            style={styles.avatar}
          />
        </TouchableOpacity>
        <AppTextBold>
          { user.name }
        </AppTextBold>
      </View>
      { renderSettings() }
    </ScrollView>
  )
}
